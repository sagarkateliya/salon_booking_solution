const response = require('../../utils/response');
const statusCode = require('../../utils/statuscode');
const messages = require('../../utils/messages.json');

const userService = require('../services/userService');
const { config } = require('process');

class DropdownController {
  async user_register(req, res) {
    try {
      let data = await userService.user_register(req.body);
      response.success(res, messages.registerMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    }
    catch (error) {
      console.log(`user_register controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async user_login(req, res) {
    try {
      let data = await userService.user_login(req.body);
      response.success(res, messages.loginMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    }
    catch (error) {
      console.log(`user_login controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_register(req, res) {
    try {
      let data = await userService.salon_register(req.body, req.file.filename);
      response.success(res,messages.registerMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_register controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_login(req, res) {
    try {
      let data = await userService.salon_login(req.body);
      response.success(res, messages.loginMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_login controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async sendotp(req, res) {
    try {
      let data = await userService.sendotp(req.body);
      response.success(res, messages.otpsendMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`sendotp controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async forgot_password(req, res) {
    try {
      let data = await userService.forgot_password(req.body);
      response.success(res, messages.otpsendMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`forgot_password controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async verifyotp(req, res) {
    try {
      let data = await userService.verifyotp(req.body);
      response.success(res, messages.otpVerifiedMsg, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`verifyotp controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async new_password(req, res) {
    try {
      let data = await userService.new_password(req.body);
      response.success(res, messages.changePassword, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`new_password controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async reset_password(req, res) {
    try {
      let data = await userService.reset_password(req.body, req.headers);
      response.success(res, messages.changePassword, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`reset_password controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_category(req, res) {
    try {
      let data = await userService.add_category(req.body, req.headers);
      response.success(res, messages.addCategory, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_category controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async get_category(req, res) {
    try {
      let data = await userService.get_category(req.body, req.headers);
      response.success(res, messages.getCategory, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`get_category controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_service(req, res) {
    try {
      let data = await userService.add_service(req.body, req.headers);
      response.success(res, messages.addService, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async my_service(req, res) {
    try {
      let data = await userService.my_service(req.body, req.headers);
      response.success(res, messages.getService, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`my_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async edit_service(req, res) {
    try {
      let data = await userService.edit_service(req.body, req.headers);
      response.success(res, messages.editService, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`edit_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async dlt_service(req, res) {
    try {
      let data = await userService.dlt_service(req.body, req.headers);
      response.success(res, messages.dltService, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`dlt_service controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_barber(req, res) {
    try {
      let data = await userService.add_barber(req.body, req.headers);
      response.success(res, messages.addBarber, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_barber controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async my_staff(req, res) {
    try {
      let data = await userService.my_staff(req.body, req.headers);
      response.success(res, messages.getBarber, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`my_staff controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async edit_barber(req, res) {
    try {
      let data = await userService.edit_barber(req.body, req.headers);
      response.success(res, messages.editBarber, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`edit_barber controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async delete_barber(req, res) {
    try {
      let data = await userService.delete_barber(req.body, req.headers);
      response.success(res, messages.deleteBarber, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`delete_barber controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async barber_availability(req, res) {
    try {
      let data = await userService.barber_availability(req.body, req.headers);
      response.success(res, messages.barberAvailability, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`barber_availability controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async refresh_token(req, res) {
    try {
      let data = await userService.refresh_token(req.headers);
      response.success(res, messages.refreshToken, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`refresh_token controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async list_salon_business_hours_weekly_schedule(req, res) {
    try {
      let data = await userService.list_salon_business_hours_weekly_schedule(req.body, req.headers);
      response.success(res, messages.listSalonBusinessHoursWeekly, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`list_salon_business_hours_weekly_schedule controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_salon_business_hours(req, res) {
    try {
      let data = await userService.add_salon_business_hours(req.body, req.headers);
      response.success(res, messages.addSalonBusinessHours, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_salon_business_hours controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async list_salon_business_hours_daily_schedule(req, res) {
    try {
      let data = await userService.list_salon_business_hours_daily_schedule(req.body, req.headers);
      response.success(res, messages.listSalonBusinessHoursDaily, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`list_salon_business_hours_daily_schedule controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_business_hours_availability(req, res) {
    try {
      let data = await userService.salon_business_hours_availability(req.body, req.headers);
      response.success(res, messages.salonBusinessHoursAvailability, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_business_hours_availability controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async list_barber_business_hours_weekly_schedule(req, res) {
    try {
      let data = await userService.list_barber_business_hours_weekly_schedule(req.body, req.headers);
      response.success(res, messages.listBarberBusinessHoursWeekly, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`list_barber_business_hours_weekly_schedule controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_barber_business_hours(req, res) {
    try {
      let data = await userService.add_barber_business_hours(req.body, req.headers);
      response.success(res, messages.addBarberBusinessHours, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_barber_business_hours controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async list_barber_business_hours_daily_schedule(req, res) {
    try {
      let data = await userService.list_barber_business_hours_daily_schedule(req.body, req.headers);
      response.success(res, messages.listBarberBusinessHoursDaily, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`list_barber_business_hours_daily_schedule controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async barber_business_hours_availability(req, res) {
    try {
      let data = await userService.barber_business_hours_availability(req.body, req.headers);
      response.success(res, messages.barberBusinessHoursAvailability, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`barber_business_hours_availability controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async edit_user_profile(req, res) {
    try {
      let data = await userService.edit_user_profile(req.body, req.headers);
      response.success(res, messages.editUserProfile, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`edit_user_profile controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async edit_salon_profile(req, res) {
    try {
      let data = await userService.edit_salon_profile(req.body, req.file);
      response.success(res, messages.editSalonProfile, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`edit_salon_profile controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async contact_us(req, res) {
    try {
      let data = await userService.contact_us(req.body, req.headers);
      response.success(res, messages.contactUs, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`contact_us controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async referral_code(req, res) {
    try {
      let data = await userService.referral_code(req.body, req.headers);
      response.success(res, messages.referralCode, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`referral_code controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async search_salon(req, res) {
    try {
      let data = await userService.search_salon(req.body, req.headers);
      response.success(res, messages.searchSalon, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`search_salon controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async like_dislike_salon(req, res) {
    try {
      let data;
      let message = await userService.like_dislike_salon(req.body, req.headers);
      response.success(res, message, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`like_dislike_salon controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async my_favorite_salons(req, res) {
    try {
      let data = await userService.my_favorite_salons(req.body, req.headers);
      response.success(res, messages.myFavSalons, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`my_favorite_salons controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async write_review(req, res) {
    try {
      let data = await userService.write_review(req.body, req.headers);
      response.success(res, messages.writeReview, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`write_review controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async list_review(req, res) {
    try {
      let data = await userService.list_review(req.body, req.headers);
      response.success(res, messages.listReview, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`list_review controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_profile_details(req, res) {
    try {
      let data = await userService.salon_profile_details(req.body, req.headers);
      response.success(res, messages.salonProfileDetails, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_profile_details controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_image_video(req, res) {
    try {
      let data = await userService.add_image_video(req.body, req.files);
      response.success(res, messages.addImageVideo, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_image_video controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async get_image_video(req, res) {
    try {
      let data = await userService.get_image_video(req.body, req.files);
      response.success(res, messages.getImageVideo, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`get_image_video controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async delete_image_video(req, res) {
    try {
      let data = await userService.delete_image_video(req.body, req.files);
      response.success(res, messages.dltImageVideo, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`delete_image_video controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_profile_gallery(req, res) {
    try {
      let data = await userService.salon_profile_gallery(req.body, req.headers);
      response.success(res, messages.salonProfileGallery, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_profile_gallery controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async order_services(req, res) {
    try {
      let data = await userService.order_services(req.body, req.headers);
      response.success(res, messages.getService, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`order_services controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async order_barber(req, res) {
    try {
      let data = await userService.order_barber(req.body, req.headers);
      response.success(res, messages.getBarber, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`order_barber controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async order_date_time(req, res) {
    try {
      let data = await userService.order_date_time(req.body, req.headers);
      response.success(res, messages.getDateTime, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`order_date_time controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_order(req, res) {
    try {
      let data = await userService.salon_order(req.body, req.headers);
      response.success(res, messages.salonOrder, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_order controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async user_appointments(req, res) {
    try {
      let data = await userService.user_appointments(req.body, req.headers);
      response.success(res, messages.getUserAppointments, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`user_appointments controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_appointments(req, res) {
    try {
      let data = await userService.salon_appointments(req.body, req.headers);
      response.success(res, messages.getSalonAppointments, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_appointments controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async order_completed(req, res) {
    try {
      let data = await userService.order_completed(req.body, req.headers);
      response.success(res, messages.orderCompleted, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`order_completed controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async order_cancelled(req, res) {
    try {
      let data = await userService.order_cancelled(req.body, req.headers);
      response.success(res, messages.orderCancelled, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`order_cancelled controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async view_review(req, res) {
    try {
      let data = await userService.view_review(req.body, req.headers);
      response.success(res, messages.viewReview, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`view_review controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_appointment_calendar(req, res) {
    try {
      let data = await userService.salon_appointment_calendar(req.body, req.headers);
      response.success(res, messages.getSalonAppCalendar, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_appointment_calendar controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async barber_appointment_calendar(req, res) {
    try {
      let data = await userService.barber_appointment_calendar(req.body, req.headers);
      response.success(res, messages.getBarberAppCalendar, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`barber_appointment_calendar controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async user_device_relation(req, res) {
    try {
      let data = await userService.user_device_relation(req.body, req.headers);
      response.success(res, 'SUCCESS', req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`user_device_relation controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async get_notification(req, res) {
    try {
      let data = await userService.get_notification(req.body, req.headers);
      response.success(res, messages.getNotification, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`get_notification controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async delete_notification(req, res) {
    try {
      let data = await userService.delete_notification(req.body, req.headers);
      response.success(res, messages.dltNotification, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`delete_notification controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async check_order() {
    try {
      await userService.check_order();
      console.log('Successfully update records using node cron');
    } catch (error) {
      console.log(`check_order controller catch error ->> ${error.message}`);
    }
  }

  async my_earnings(req, res) {
    try {
      let data = await userService.my_earnings(req.body, req.headers);
      response.success(res, messages.getMyEarnings, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`my_earnings controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async user_dashboard(req, res) {
    try {
      let data = await userService.user_dashboard(req.body, req.headers);
      response.success(res, messages.userDashboard, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`user_dashboard controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async salon_dashboard(req, res) {
    try {
      let data = await userService.salon_dashboard(req.body, req.headers);
      response.success(res, messages.salonDashboard, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`salon_dashboard controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async add_offer(req, res) {
    try {
      let data = await userService.add_offer(req.body, req.file.filename);
      response.success(res, messages.addOffer, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`add_offer controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async get_offer(req, res) {
    try {
      let data = await userService.get_offer(req.body, req.headers);
      response.success(res, messages.getOffer, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`get_offer controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

  async dlt_offer(req, res) {
    try {
      let data = await userService.dlt_offer(req.body, req.headers);
      response.success(res, messages.dltOffer, req.headers.language, data, statusCode.success, req.headers.is_openpgp);
    } catch (error) {
      console.log(`dlt_offer controller catch error ->> ${error.message}`);

      if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
      else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
    }
  }

}

module.exports = new DropdownController();