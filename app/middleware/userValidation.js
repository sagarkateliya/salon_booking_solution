const joi = require('joi');
const passwordComplexity = require('joi-password-complexity');
const jwt = require('jsonwebtoken');
const { User, Salon_business_hours } = require('../../models');
const config = require('../../config');
const response = require('../../utils/response');
const { Op } = require('sequelize');
const { convertTimeFormat } = require('../../common/common');

class userValidation {
  userRegisterSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          user_type, name, email, country_code, mobile, password, gender
        } = req.body;
        const userRegisterSchema = joi.object({
          user_type: joi.number().required().label('User type'),
          name: joi.string().required().label('Name'),
          email: joi.string().required().label('Email'),
          country_code: joi.string().required().label('Country_code'),
          mobile: joi.string().required().label('Mobile'),
          password: passwordComplexity({
            min: 8,
            max: 32,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).required().label('Password'),
          gender: joi.number().allow(1,2).required().label('Gender')
        })
        await userRegisterSchema.validateAsync(
          {user_type, name, email, country_code, mobile, password, gender},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`userRegisterSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
  
  loginSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          email, password
        } = req.body;
        const loginSchema = joi.object({
          email: joi.string().required().label('Email'),
          password: passwordComplexity({
            min: 8,
            max: 32,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).required().label('Password')
        });
        await loginSchema.validateAsync({email, password}, {errors: {wrap: {label: ''}}});
        next();
      }
      catch(error) {
        console.log(`loginSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
  
  salonRegisterSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          user_type, name, email, country_code, mobile, password, salon_type, salon_name, address, service_gender
        } = req.body;
        const salonRegisterSchema = joi.object({
          user_type: joi.number().required().label('User type'),
          name: joi.string().required().label('Name'),
          email: joi.string().required().label('Email'),
          country_code: joi.string().required().label('Country_code'),
          mobile: joi.string().required().label('Mobile'),
          password: passwordComplexity({
            min: 8,
            max: 32,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).required().label('Password'),
          salon_type: joi.number().allow(1,2).required().label('Salon type'),
          salon_name: joi.string().required().label('Salon name'),
          address: joi.string().required().label('Address'),
          service_gender: joi.number().allow(1,2).required().label('Service gender')
        })
        await salonRegisterSchema.validateAsync(
          {user_type, name, email, country_code, mobile, password, salon_type, salon_name, address, service_gender},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`salonRegisterSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  emailSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { email } = req.body;
        const emailSchema = joi.object({
          email: joi.string().email().required().label('Email'),
        });
        await emailSchema.validateAsync({ email }, {errors: {wrap: {label: ''}}});
        next();
      }
      catch(error) {
        console.log(`emailSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
  
  newPassSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { new_password } = req.body;
        const newPassSchema = joi.object({
          new_password: passwordComplexity({
            min: 8,
            max: 32,
            lowerCase: 1,
            upperCase: 1,
            numeric: 1,
            symbol: 1,
            requirementCount: 4
          }).required().label('Password')
        });
        await newPassSchema.validateAsync({ new_password }, {errors: {wrap: {label: ''}}});
        next();
      }
      catch(error) {
        console.log(`newPassSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
  
  nameSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { name } = req.body;
        const nameSchema = joi.object({
          name: joi.string().required().label('Name')
        });
        await nameSchema.validateAsync({ name }, {errors: {wrap: {label: ''}}});
        next();
      }
      catch(error) {
        console.log(`nameSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  serviceSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          name, category_id, price, time, description
        } = req.body;
        const serviceSchema = joi.object({
          name: joi.string().required().label('Name'),
          category_id: joi.required().label('Category'),
          price: joi.number().required().label('Price'),
          time: joi.string().required().label('Estimated time'),
          description: joi.string().required().label('Description')
        });
        await serviceSchema.validateAsync(
          {name, category_id, price, time, description},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`serviceSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
  
  editServiceSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          _id, name, category_id, price, time, description
        } = req.body;
        const editServiceSchema = joi.object({
          _id: joi.number().required().label('Id'),
          name: joi.string().required().label('Name'),
          category_id: joi.required().label('Category'),
          price: joi.number().required().label('Price'),
          time: joi.string().required().label('Estimated time'),
          description: joi.string().required().label('Description')
        })
        await editServiceSchema.validateAsync(
          {_id, name, category_id, price, time, description},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`editServiceSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  addSalonBusinessHoursSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          schedule_type, day, working_start_time, working_end_time, break_start_time, break_end_time, available, date
        } = req.body

        if(schedule_type == 1) {
          const addSalonBusinessHoursSchema = joi.object({
            schedule_type: joi.number().required().label('Schedule_type'),
            day: joi.number().greater(0).less(8).required().label('Day'),
            working_start_time: joi.string().allow(null, "").label('Working start time'),
            working_end_time: joi.string().allow(null, "").label('Working end time'),
            break_start_time: joi.string().allow(null, "").label('Break start time'),
            break_end_time: joi.string().allow(null, "").label('Break end time'),
            available: joi.number().required().label('Available')
          })
          await addSalonBusinessHoursSchema.validateAsync(
            {schedule_type, day, working_start_time, working_end_time, break_start_time, break_end_time, available},
            {errors: {wrap: {label: ''}}}
          );
        }
        else {
          const addSalonBusinessHoursSchema = joi.object({
            schedule_type: joi.number().required().label('Schedule_type'),
            day: joi.number().greater(0).less(8).required().label('Day'),
            date: joi.date().required().label('Date'),
            working_start_time: joi.string().allow(null, "").label('Working start time'),
            working_end_time: joi.string().allow(null, "").label('Working end time'),
            break_start_time: joi.string().allow(null, "").label('Break start time'),
            break_end_time: joi.string().allow(null, "").label('Break end time'),
            available: joi.number().required().label('Available')
          })
          await addSalonBusinessHoursSchema.validateAsync(
            {schedule_type, day, date, working_start_time, working_end_time, break_start_time, break_end_time, available},
            {errors: {wrap: {label: ''}}}
          );
        }
        
        next();
      }
      catch(error) {
        console.log(`addSalonBusinessHoursSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  salonWorkingHoursValidation(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          working_start_time, working_end_time, break_start_time, break_end_time
        } = req.body
        let salonWorkingStartTime = await convertTimeFormat(working_start_time);
        let salonWorkingEndTime = await convertTimeFormat(working_end_time);
        let salonBreakStartTime = await convertTimeFormat(break_start_time);
        let salonBreakEndTime = await convertTimeFormat(break_end_time);

        if(salonBreakStartTime && salonBreakEndTime == "") {
          throw {statusCode:200, message: 'Please add break end time'}
        }
        else if(salonBreakStartTime == "" && salonBreakEndTime) {
          throw {statusCode:200, message:'Please add break start time'}
        }
        else if(salonBreakStartTime == "") {}
        else if(
          salonBreakStartTime < salonWorkingStartTime ||
          salonBreakStartTime > salonWorkingEndTime ||
          salonBreakEndTime < salonWorkingStartTime ||
          salonBreakEndTime > salonWorkingEndTime
        ) {
          throw {statusCode:200, message:'Please enter your break time between your working time!'}
        }
        else if( salonBreakEndTime < salonBreakStartTime ) {
          throw {statusCode:200, message:'Break end time is after break start!'}
        }
        next();
      }
      catch(error) {
        console.log(`salonWorkingHoursValidation validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  listSalonBusinessHoursSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { schedule_type, date } = req.body

        const listSalonBusinessHoursSchema = joi.object({
          schedule_type: joi.number().required().label('Schedule_type'),
          date: joi.date().required().label('Date')
        })
        await listSalonBusinessHoursSchema.validateAsync( {schedule_type, date}, {errors: {wrap: {label: ''}}} );
        next();
      }
      catch(error) {
        console.log(`listSalonBusinessHoursSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  addBarberBusinessHoursSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          barber_id, schedule_type, day, working_start_time, working_end_time, break_start_time, break_end_time, available, date
        } = req.body

        if(schedule_type == 1) {
          const addSalonBusinessHoursSchema = joi.object({
            barber_id: joi.number().required().label('Barber id'),
            schedule_type: joi.number().required().label('Schedule_type'),
            day: joi.number().greater(0).less(8).required().label('Day'),
            working_start_time: joi.string().allow(null, "").label('Working start time'),
            working_end_time: joi.string().allow(null, "").label('Working end time'),
            break_start_time: joi.string().allow(null, "").label('Break start time'),
            break_end_time: joi.string().allow(null, "").label('Break end time'),
            available: joi.number().required().label('Available')
          })
          await addSalonBusinessHoursSchema.validateAsync(
            {barber_id, schedule_type, day, working_start_time, working_end_time, break_start_time, break_end_time, available},
            {errors: {wrap: {label: ''}}}
          );
        }
        else {
          const addSalonBusinessHoursSchema = joi.object({
            barber_id: joi.number().required().label('Barber id'),
            schedule_type: joi.number().required().label('Schedule_type'),
            day: joi.number().greater(0).less(8).required().label('Day'),
            date: joi.date().required().label('Date'),
            working_start_time: joi.string().allow(null, "").label('Working start time'),
            working_end_time: joi.string().allow(null, "").label('Working end time'),
            break_start_time: joi.string().allow(null, "").label('Break start time'),
            break_end_time: joi.string().allow(null, "").label('Break end time'),
            available: joi.number().required().label('Available')
          })
          await addSalonBusinessHoursSchema.validateAsync(
            {barber_id, schedule_type, day, date, working_start_time, working_end_time, break_start_time, break_end_time, available},
            {errors: {wrap: {label: ''}}}
          );
        }
        
        next();
      }
      catch(error) {
        console.log(`addBarberBusinessHoursSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  barberWorkingHoursValidation(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          salon_id, barber_id, schedule_type, day, working_start_time, working_end_time, break_start_time, break_end_time, date
        } = req.body
        let barberWorkingStartTime = await convertTimeFormat(working_start_time);
        let barberWorkingEndTime = await convertTimeFormat(working_end_time);
        let barberBreakStartTime = await convertTimeFormat(break_start_time);
        let barberBreakEndTime = await convertTimeFormat(break_end_time);

        if(schedule_type == 1) {
          let salon_weekly_working_time = await Salon_business_hours.findOne({
            attributes: ['_id', 'working_start_time', 'working_end_time'],
            where: {
              [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {day: day}]
            }
          });

          if(salon_weekly_working_time == null) {
            throw{statusCode:200, message:'This day salon is closed'}
          }
          let salonWorkingStartTime = await convertTimeFormat(salon_weekly_working_time.working_start_time);
          let salonWorkingEndTime = await convertTimeFormat(salon_weekly_working_time.working_end_time);

          if(
            barberWorkingStartTime < salonWorkingStartTime ||
            barberWorkingStartTime > salonWorkingEndTime ||
            barberWorkingEndTime < salonWorkingStartTime ||
            barberWorkingEndTime > salonWorkingEndTime
          ) {
            throw {statusCode:200, message:'Please enter your working time between salon working time!'}
          }
          else if(barberBreakStartTime && barberBreakEndTime == "") {
            throw {statusCode:200, message:'Please add break end time'}
          }
          else if(barberBreakStartTime == "" && barberBreakEndTime) {
            throw {statusCode:200, message:'Please add break start time'}
          }
          else if(barberBreakStartTime == "") {}
          else if(
            barberBreakStartTime < barberWorkingStartTime ||
            barberBreakStartTime > barberWorkingEndTime ||
            barberBreakEndTime < barberWorkingStartTime ||
            barberBreakEndTime > barberWorkingEndTime
          ) {
            throw {statusCode:200, message:'Please enter your break time between your working time!'}
          }
          else if( barberBreakEndTime < barberBreakStartTime ) {
            throw {statusCode:200, message:'Break end time is after break start!'}
          }
        }
        else {
          let salon_daily_working_time = await Salon_business_hours.findOne({
            attributes: ['_id', 'working_start_time', 'working_end_time'],
            where: {
              [Op.or]: [{
                [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {date: date}]
              },{
                [Op.and] : [{schedule_type: 1}, {salon_id: salon_id}, {day: day}]
              }]
            }
          });

          if(salon_daily_working_time == null) {
            throw{statusCode:200, message:'This day salon is closed'}
          }
          let salonWorkingStartTime = await convertTimeFormat(salon_daily_working_time.working_start_time);
          let salonWorkingEndTime = await convertTimeFormat(salon_daily_working_time.working_end_time);

          if(
            barberWorkingStartTime < salonWorkingStartTime ||
            barberWorkingStartTime > salonWorkingEndTime ||
            barberWorkingEndTime < salonWorkingStartTime ||
            barberWorkingEndTime > salonWorkingEndTime
          ) {
            throw {statusCode:200, message:'Please enter your working time between salon working time!'}
          }
          else if(barberBreakStartTime && barberBreakEndTime == "") {
            throw {statusCode:200, message:'Please add break end time'}
          }
          else if(barberBreakStartTime == "" && barberBreakEndTime) {
            throw {statusCode:200, message:'Please add break start time'}
          }
          else if(barberBreakStartTime == "") {}
          else if(
            barberBreakStartTime < barberWorkingStartTime ||
            barberBreakStartTime > barberWorkingEndTime ||
            barberBreakEndTime < barberWorkingStartTime ||
            barberBreakEndTime > barberWorkingEndTime
          ) {
            throw {statusCode:200, message:'Please enter your break time between your working time!'}
          }
          else if( barberBreakEndTime < barberBreakStartTime ) {
            throw {statusCode:200, message:'Break end time is after break start!'}
          }
        }
        next();
      }
      catch(error) {
        console.log(`barberWorkingHoursValidation validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  listBarberBusinessHoursSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { barber_id, schedule_type, date } = req.body

        const listBarberBusinessHoursSchema = joi.object({
          barber_id: joi.number().required().label('Barber id'),
          schedule_type: joi.number().required().label('Schedule_type'),
          date: joi.date().required().label('Date')
        })
        await listBarberBusinessHoursSchema.validateAsync( {barber_id, schedule_type, date}, {errors: {wrap: {label: ''}}} );
        next();
      }
      catch(error) {
        console.log(`listBarberBusinessHoursSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  editUserProfileSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          name, email, country_code, mobile, gender
        } = req.body;
        const editServiceSchema = joi.object({
          name: joi.string().required().label('Name'),
          email: joi.string().required().label('Email'),
          country_code: joi.string().required().label('Country_code'),
          mobile: joi.string().required().label('Mobile'),
          gender: joi.number().allow(1,2).required().label('Gender')
        })
        await editServiceSchema.validateAsync(
          {name, email, country_code, mobile, gender},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`editUserProfileSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  editSalonProfileSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          name, country_code, mobile, salon_type, salon_name, address, service_gender, description
        } = req.body;
        const editServiceSchema = joi.object({
          name: joi.string().label('Name'),
          country_code: joi.string().label('Country_code'),
          mobile: joi.string().label('Mobile'),
          salon_type: joi.number().allow(1,2).label('Salon type'),
          salon_name: joi.string().label('Salon name'),
          address: joi.string().label('Address'),
          service_gender: joi.number().allow(1,2).label('Service gender'),
          description: joi.string().label('Description')
        })
        await editServiceSchema.validateAsync(
          {name, country_code, mobile, salon_type, salon_name, address, service_gender, description},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`editUserProfileSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  contactUsSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          name, email, country_code, mobile, subject, message
        } = req.body;
        const editServiceSchema = joi.object({
          name: joi.string().required().label('Name'),
          email: joi.string().email().required().label('Email'),
          country_code: joi.string().required().label('Country_code'),
          mobile: joi.string().required().label('Mobile'),
          subject: joi.string().required().label('Subject'),
          message: joi.string().required().label('Message')
        })
        await editServiceSchema.validateAsync(
          {name, email, country_code, mobile, subject, message},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`editUserProfileSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  searchSalonSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          search_type, address, salon_or_barber_name, salon_type, service_gender
        } = req.body;
        const searchSalonSchema = joi.object({
          search_type: joi.number().required().label('Search type'),
          address: joi.string().label('Address'),
          salon_or_barber_name: joi.string().label('Salon or barber name'),
          salon_type: joi.number().required().label('Salon type'),
          service_gender: joi.number().required().label('Service gender')
        })
        await searchSalonSchema.validateAsync(
          {search_type, address, salon_or_barber_name, salon_type, service_gender},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`searchSalonSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  writeReviewSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          salon_id, price, value, quality, friendliness, cleanliness, description
        } = req.body;
        const searchSalonSchema = joi.object({
          salon_id: joi.number().required().label('Salon id'),
          price: joi.number().less(6).label('Price'),
          value: joi.number().less(6).label('Value'),
          quality: joi.number().less(6).label('Quality'),
          friendliness: joi.number().less(6).label('Friendliness'),
          cleanliness: joi.number().less(6).label('Cleanliness'),
          description: joi.string().label('Description')
        })
        await searchSalonSchema.validateAsync(
          { salon_id, price, value, quality, friendliness, cleanliness, description },
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`writeReviewSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  salonOrder(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          service_id, barber_id, order_date, order_time, is_offer, total_price,
          walk_in_user_name, walk_in_user_cc, walk_in_user_number
        } = req.body

        const listBarberBusinessHoursSchema = joi.object({
          service_id: joi.array().items(joi.number().required()).required().label('Service id'),
          barber_id: joi.number().required().label('Barber id'),
          order_date: joi.date().required().label('Order date'),
          order_time: joi.string().required().label('Order time'),
          is_offer: joi.number().label('Is offer'),
          total_price: joi.number().required().label('Total price'),
          walk_in_user_name: joi.string().label('User name'),
          walk_in_user_number: joi.string().label('User number'),
          walk_in_user_cc: joi.string().label('User country code')
        })
        await listBarberBusinessHoursSchema.validateAsync(
          {service_id, barber_id, order_date, order_time, is_offer, total_price,
            walk_in_user_name, walk_in_user_cc, walk_in_user_number},
          {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`salonOrder validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  userDeviceSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {device_token} = req.body;
        let {device_id, device_type} = req.headers;

        const userDeviceSchema = joi.object({
          device_token: joi.string().required().label('Device token'),
          device_id: joi.string().required().label('Device id'),
          device_type: joi.number().required().label('Device type')
        })
        await userDeviceSchema.validateAsync(
          {device_token, device_id, device_type}, {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`userDeviceSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  myEarningSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {start_date, end_date} = req.body;

        const myEarningSchema = joi.object({
          start_date: joi.date().required().label('Start date'),
          end_date: joi.date().required().label('End date')
        })
        await myEarningSchema.validateAsync(
          {start_date, end_date}, {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`myEarningSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  offerSchema(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let {salon_id, service_id, discount, description} = req.body;

        const offerSchema = joi.object({
          salon_id: joi.number().required().label('Salon id'),
          service_id: joi.number().required().label('Service id'),
          discount: joi.number().required().label('Discount'),
          description: joi.string().required().label('Description'),
        })
        await offerSchema.validateAsync(
          {salon_id, service_id, discount, description}, {errors: {wrap: {label: ''}}}
        );
        next();
      }
      catch(error) {
        console.log(`offerSchema validation catch error ->> ${error.message}`);

        if (error.message)
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501
          );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  verifyToken(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let{ token } = req.headers

        if(!token) {
          throw {statusCode:200, message: 'Token is require!'}
        }
        let decoded = jwt.verify(token, config.jwtSecretKey); //Verify token;
  
        let user_id = decoded.user_id; //Get id from decoded object;
        req.body.user_id = user_id
        next();
      }
      catch(error) {
        if (error.message == 'Token is require!' || error.message == 'jwt expired') {
          if(error.message == 'jwt expired') {
            error = Error('TOKEN_EXPIRED');
          }
          console.log('verifyToken if error ==>>', error.message);
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501)
        }
        else {
          error = Error('Invalid token');
          error.statusCode = 200;
          console.log('verifyToken else error ==>>', error);
          response.error(
            res,
            error.message || 'something went wrong',
            req.headers.language,
            error.statusCode ? error.statusCode : 403 || 501)
        }
      }
    });
  }

  userType1(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { user_id } = req.body;

        // Fetch user type from users table:
        const userType = await User.findOne({
          attributes: ['user_type'],
          where: {_id: user_id}
        });

        if(userType.user_type != 1) {
          throw {statusCode:200, message:'You are not customer!'} //Throw error if user is salon side user;
        }
        next();
      }
      catch(error) {
        console.log(`userType1 validation catch error ->> ${error.message}`);
        if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }

  userType2(req, res, next) {
    return new Promise(async (resolve, reject) => {
      try{
        let { user_id } = req.body;

        // Fetch user type from users table:
        const userType = await User.findAll({
          attributes: ['user_type'],
          where: {_id: user_id}
        });

        if(userType[0].user_type != 2) {
          throw {statusCode:200, message:'You are not salon side user!'} //Throw error if user is customer;
        }

        req.body.salon_id = user_id;
        delete req.body.user_id
        next();
      }
      catch(error) {
        console.log(`userType2 validation catch error ->> ${error.message}`);
        if (error.message)
        response.error(
          res,
          error.message || 'something went wrong',
          req.headers.language,
          error.statusCode ? error.statusCode : 403 || 501
        );
        else response.error(res, error, req.headers.language, statusCode.error, req.headers.is_openpgp);
      }
    });
  }
}

module.exports = new userValidation();