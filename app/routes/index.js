const path = require('path');
const Multer = require('multer');

const userController = require('../controllers/userController');
const userValidation = require('../middleware/userValidation');
const response = require('../../utils/response');

const storage = Multer.diskStorage({
    // Destination to store image
    destination: (req, file, cb) => {
      if(file.fieldname == 'profile_pic') {
        cb(null, "images");
      }
      else if(file.fieldname == 'images_videos') {
        cb(null, "storage/salon_image_video")
      }
      else if(file.fieldname == 'offer_image') {
        cb(null, "storage/offer_images")
      }
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + path.extname(file.originalname))
      // file.fieldname is name of the field (image)
      // path.extname get the uploaded file extension
    }
});

const upload = Multer({
  storage: storage,
  limits: {
    fileSize: 10000000 // 1000000 bytes = 1 MB
  },
  // fileFilter(req, file, cb) {
  //   console.log("========", new Date());
  //   if(!file.originalname.match(/\.(png|jpg|jpeg)$/)) {
  //     // upload only png and jpg format
  //     return cb(new Error('Please upload a Image'))
  //   }
  //   cb(undefined, true)
  // }
});

const  fileSizeLimit = (error, req, res, next) => {
  if(error) {
    response.error(
    res,
    error.message || 'something went wrong',
    req.headers.language,
    error.statusCode ? error.statusCode : 200)
  } else { next() }
}

exports.routerConfig = (app) => {
  app.post('/user_register',
    userValidation.userRegisterSchema,
    userController.user_register
  )
  app.post('/user_login',
    userValidation.loginSchema,
    userController.user_login
  )
  app.post('/salon_register',
    upload.single('profile_pic'),
    fileSizeLimit,
    userValidation.salonRegisterSchema,
    userController.salon_register
  )
  app.post('/salon_login', 
    userValidation.loginSchema,
    userController.salon_login
  )

  app.post('/sendotp',
    userController.sendotp
  )
  app.post('/forgot_password',
    userValidation.emailSchema,
    userController.forgot_password
  )
  app.post('/verifyotp',
    userController.verifyotp
  )
  app.post('/new_password',
    userValidation.newPassSchema,
    userController.new_password
  )
  app.post('/reset_password',
    userValidation.verifyToken,
    userValidation.newPassSchema,
    userController.reset_password
  )

  app.post('/add_category',
    userValidation.nameSchema,
    userController.add_category
  )
  app.post('/get_category',
    userValidation.verifyToken,
    userController.get_category
  )

  app.post('/add_service',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.serviceSchema,
    userController.add_service
  )
  app.post('/my_service',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.my_service
  )
  app.post('/edit_service',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.editServiceSchema,
    userController.edit_service
  )
  app.post('/dlt_service',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.dlt_service
  )

  app.post('/add_barber',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.nameSchema,
    userController.add_barber
  )
  app.post('/my_staff',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.my_staff
  )
  app.post('/edit_barber',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.nameSchema,
    userController.edit_barber
  )
  app.post('/delete_barber',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.delete_barber
  )
  app.post('/barber_availability',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.barber_availability
  )

  app.post('/refresh_token',
    userController.refresh_token
  )

  app.post('/list_salon_business_hours_weekly_schedule',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.list_salon_business_hours_weekly_schedule
  )
  app.post('/add_salon_business_hours',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.addSalonBusinessHoursSchema,
    userValidation.salonWorkingHoursValidation,
    userController.add_salon_business_hours
  )
  app.post('/list_salon_business_hours_daily_schedule',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.listSalonBusinessHoursSchema,
    userController.list_salon_business_hours_daily_schedule
  )
  app.post('/salon_business_hours_availability',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.salon_business_hours_availability
  )

  app.post('/list_barber_business_hours_weekly_schedule',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.list_barber_business_hours_weekly_schedule
  )
  app.post('/add_barber_business_hours',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.addBarberBusinessHoursSchema,
    userValidation.barberWorkingHoursValidation,
    userController.add_barber_business_hours
  )
  app.post('/list_barber_business_hours_daily_schedule',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.listBarberBusinessHoursSchema,
    userController.list_barber_business_hours_daily_schedule
  )
  app.post('/barber_business_hours_availability',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.barber_business_hours_availability
  )

  app.post('/edit_user_profile',
    userValidation.verifyToken,
    userValidation.userType1,
    userValidation.editUserProfileSchema,
    userController.edit_user_profile
  )
  app.post('/edit_salon_profile',
    upload.single('profile_pic'),
    fileSizeLimit,
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.editSalonProfileSchema,
    userController.edit_salon_profile
  )
  app.post('/contact_us',
    userValidation.verifyToken,
    userValidation.contactUsSchema,
    userController.contact_us
  )
  app.post('/referral_code',
    userValidation.verifyToken,
    userController.referral_code
  )

  app.post('/search_salon', 
    userValidation.verifyToken,
    userValidation.userType1,
    userValidation.searchSalonSchema,
    userController.search_salon
  )
  app.post('/like_dislike_salon',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.like_dislike_salon
  )
  app.post('/my_favorite_salons',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.my_favorite_salons
  )
  app.post('/write_review', 
    userValidation.verifyToken,
    userValidation.userType1,
    userValidation.writeReviewSchema,
    userController.write_review
  )
  app.post('/list_review', 
    userValidation.verifyToken,
    userController.list_review
  )

  app.post('/salon_profile_details',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.salon_profile_details
  )
  app.post('/add_image_video',
    upload.array('images_videos'),
    fileSizeLimit,
    userValidation.verifyToken,
    userValidation.userType2,
    userController.add_image_video
  )
  app.post('/get_image_video',
    userValidation.verifyToken,
    userController.get_image_video
  )
  app.post('/delete_image_video',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.delete_image_video
  )
  app.post('/salon_profile_gallery',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.salon_profile_gallery
  )

  app.post('/order_services',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.order_services
  )
  app.post('/order_barber',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.order_barber
  )
  app.post('/order_date_time',
    userValidation.verifyToken,
    userController.order_date_time
  )
  app.post('/salon_order',
    userValidation.verifyToken,
    userValidation.salonOrder,
    userController.salon_order
  )

  app.post('/user_appointments',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.user_appointments
  )
  app.post('/salon_appointments',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.salon_appointments
  )
  app.post('/order_completed',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.order_completed
  )
  app.post('/order_cancelled',
    userValidation.verifyToken,
    userController.order_cancelled
  )
  app.post('/view_review',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.view_review
  )
  app.post('/salon_appointment_calendar',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.salon_appointment_calendar
  )
  app.post('/barber_appointment_calendar',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.barber_appointment_calendar
  )

  app.post('/user_device_relation',
    userValidation.verifyToken,
    userValidation.userDeviceSchema,
    userController.user_device_relation
  )

  app.post('/get_notification',
    userValidation.verifyToken,
    userController.get_notification
  )
  app.post('/delete_notification',
    userValidation.verifyToken,
    userController.delete_notification
  )
  app.post('/my_earnings',
    userValidation.verifyToken,
    userValidation.userType2,
    userValidation.myEarningSchema,
    userController.my_earnings
  )

  app.post('/user_dashboard',
    userValidation.verifyToken,
    userValidation.userType1,
    userController.user_dashboard
  )
  app.post('/salon_dashboard',
    userValidation.verifyToken,
    userValidation.userType2,
    userController.salon_dashboard
  )

  app.post('/add_offer',
    upload.single('offer_image'),
    fileSizeLimit,
    userValidation.offerSchema,
    userController.add_offer
  )
  app.post('/get_offer',
    userValidation.verifyToken,
    userController.get_offer
  )
  app.post('/dlt_offer',
    userController.dlt_offer
  )
};

exports.cronConfig = async () => {
  await userController.check_order()
}