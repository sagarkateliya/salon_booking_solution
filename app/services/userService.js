const fs = require('fs');
const path = require('path');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const {Op, fn, col, where, literal, cast} = Sequelize;
const referral_code_generator = require('referral-code-generator');
const messages = require('../../utils/messages.json')

const {
  getJwtToken, generateOTP, encryptedPassword, decryptedPassword, profile_pic,
  sendMails, time_slots, pagination, checkLastPage, pushNotification
} = require('../../common/common');

const {
  User, Salon_category, Salon_service, Salon_staff, Salon_business_hours, Barber_business_hours, Contact_us,
  Favorite_salon, Salon_review, Salon_images_videos, Salon_order_details, Order_service, User_device,
  Notification, Salon_offers
} = require('../../models');

const {
  defaultRefreshToken, profilePicDir, profilePicPath, imageVideoDir, imageVideoPath, offerImageDir, offerImagePath
} = require('../../config');

class UserService {
  user_register(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_type, name, email, country_code, mobile, gender, password
        } = body;

        // Check user is already registered or not:
        const user = await User.findOne({
          attributes: ['_id', 'email'],
          where: { [Op.or]: [{email: email}, {mobile: mobile}] }
        });

        if(user != null) {
          throw {statusCode:200, message:'You are already registered!'}
        }

        if(user_type == 1) {
          // Check user account is deleted or not:
          let userData = await User.findOne({
            attributes: ['_id'],
            where: { [Op.and]: [{email: email}, {is_deleted: 1}]}
          });

          if(userData != null) {
            throw {statusCode:200, message:'Your account was deleted'}
          }

          const hash = await encryptedPassword(password); //For password encryption;
          const code = await referral_code_generator.alphaNumeric('uppercase', 4, 1); //Generate referral code;
          body.password = hash;
          body.referral_code = code;
          
          // User data insert into database:
          userData = await User.create(body);
          console.log('userData ==>>', userData);

          let token = await getJwtToken(userData._id); //Generate token for that user id;
          return resolve({
            token: token,
            userData: {
              _id:userData._id,
              user_type: userData.user_type,
              name: userData.name,
              email: userData.email,
              country_code:userData.country_code,
              mobile: userData.mobile,
              gender: userData.gender,
              is_active: userData.is_active,
              is_verified: userData.is_verified,
              createdAt: userData.createdAt,
              updatedAt: userData.updatedAt
            }
          });
        }
      }
      catch(error) {
        console.log('user_register service catch error ===>>', error.message);
        return reject(error);
      }
    });
  };

  user_login(body, headers) {
    return new Promise(async (resolve, reject) => {
      try{
        let {
          email, password
        } = body;

        // Fetch user data from database if exist:
        const user = await User.findAll({
          attributes: [
            '_id', 'user_type', 'name', 'email', 'country_code', 'mobile', 'gender',
            'is_active', 'is_verified', 'createdAt', 'updatedAt', 'password'
          ],
          where: {email: email}
        });
        console.log('user ==>>', user[0]);

        // Check user was signuped or not:
        if(user.length == 0) {
          throw {statusCode:200, message:'Email does not exist'}
        }

        //  Check password is correct or not:
        if(user && await decryptedPassword(password, user[0].password) == true) {
          let token = await getJwtToken(user[0]._id); //Generate token for that user id;
          console.log('login successfull');

          return resolve({
            token: token,
            userData: {
              _id:user[0]._id,
              user_type: user[0].user_type,
              name: user[0].name,
              email: user[0].email,
              country_code:user[0].country_code,
              mobile: user[0].mobile,
              gender: user[0].gender,
              is_active: user[0].is_active,
              is_verified: user[0].is_verified,
              createdAt: user[0].createdAt,
              updatedAt: user[0].updatedAt
            }
          });
        }
        throw {statusCode:200, message:'Password is incorrect!'}; //Throw error for incorrect password;
      }
      catch(error) {
        console.log('user_login service catch error ===>>', error.message);
        return reject(error);
      }
    });
  };

  salon_register(body, filename) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_type, name, email, country_code, mobile, password, salon_type, salon_name, address, service_gender
        } = body;

        // Check user is already registered or not:
        const user = await User.findAll({
          attributes: ['_id', 'email'],
          where: { [Op.or] : [{email:email}, {mobile:mobile}] } 
        });

        if(user.length >=1) {
          throw {statusCode:200, message:'You are already registered!'}
        }

        if(user_type == 2) {
          // Check user account is deleted or not:
          let userData = await User.findAll({
            attributes: ['_id'],
            where: { [Op.and] : [{email:email}, {is_deleted: 1}]}
          });

          if(userData.length >= 1){
            throw {statusCode:200, message:'Your account was deleted'}
          }

          const hash = await encryptedPassword(password); //For password encryption;
          const code = await referral_code_generator.alphaNumeric('uppercase', 4, 1); //Generate referral code;
          body.password = hash;
          body.referral_code = code;
          body.profile_pic = filename; 
          
          // User data insert into database:
          userData = await User.create(body)
          console.log('userData ==>>', userData);

          let token = await getJwtToken(userData._id); //Generate token for that user id;

          // Set url for images:
          let image_url =  profile_pic(userData.profile_pic);

          return resolve({
            token : token, 
            userData: {
              _id:userData._id,
              user_type: userData.user_type,
              name: userData.name,
              email: userData.email,
              country_code:userData.country_code,
              mobile: userData.mobile,
              salon_type: userData.salon_type,
              salon_name: userData.salon_name,
              profile_pic: image_url,
              address: userData.address,
              service_gender: userData.service_gender,
              is_active: userData.is_active,
              is_verified: userData.is_verified,
              createdAt: userData.createdAt,
              updatedAt: userData.updatedAt
            }
          });
        }
      } catch (error) {
        console.log('salon_register service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_login(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { email, password, device_token } = body;
        // let { device_id, device_type } = headers;

        const user_type = await User.findOne({
          attributes: ['user_type'],
          where: {email: email}
        });

        if(user_type == null) throw {statusCode:200, message:'Email does not exist'};

        if(user_type.user_type == 1) {
          // Fetch user data from database if exist:
          const user = await User.findOne({
            attributes: [
              '_id', 'user_type', 'name', 'email', 'country_code', 'mobile', 'gender',
              'is_active', 'is_verified', 'createdAt', 'updatedAt', 'password'
            ],
            where: {email: email}
          });

          // Check user was signuped or not:
          if(user == null) {
            throw {statusCode:200, message:'Email does not exist'}
          }

          //  Check password is correct or not:
          if(user && await decryptedPassword(password, user.password) == true) {
            let token = await getJwtToken(user._id); //Generate token for that user id;
            console.log('login successfull');  

            return resolve({
              token: token,
              userData: {
                _id:user._id,
                user_type: user.user_type,
                name: user.name,
                email: user.email,
                country_code:user.country_code,
                mobile: user.mobile,
                gender: user.gender,
                is_active: user.is_active,
                is_verified: user.is_verified,
                createdAt: user.createdAt,
                updatedAt: user.updatedAt
              }
            });
          }
          throw {statusCode:200, message:'password is incorrect'}; //Throw error for incorrect password;
        }

        // Fetch user data from database if exist:
        const user = await User.findOne({
          attributes: [
            '_id', 'user_type', 'name', 'email', 'country_code', 'mobile', 'salon_type', 'salon_name', 'profile_pic', 
            'address', 'service_gender', 'is_active', 'is_verified', 'createdAt', 'updatedAt', 'password'
          ],
          where: {email: email}
        });

        // Check user was signuped or not:
        if(user == null) {
        throw {statusCode:200, message:'Email does not exist'}
        }

        //  Check password is correct or not:
        if(user && await decryptedPassword(password, user.password) == true) {
          let token = await getJwtToken(user._id); //Generate token for that user id;
          console.log('login successfull');

          // Set url for images:
          let image_url =  profile_pic(user.profile_pic);

          return resolve({
            token : token,
            userData: {
              _id:user._id,
              user_type: user.user_type,
              name: user.name,
              email: user.email,
              country_code:user.country_code,
              mobile: user.mobile,
              salon_type: user.salon_type,
              salon_name: user.salon_name,
              profile_pic: image_url,
              address: user.address,
              service_gender: user.service_gender,
              is_active: user.is_active,
              is_verified: user.is_verified,
              created_date: user.createdAt,
              modified_date: user.updatedAt
            }
          });
        }
        throw {statusCode:200, message:'password is incorrect'}; //Throw error for incorrect password;
      } catch (error) {
        console.log('salon_login service catch error ===>>', error.message);
        return reject(error);
      }
    })
  }

  sendotp(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          email
        } = body
        
        // Fetch email and otp from database:
        const user = await User.findAll({
          attributes: ['email', 'otp'],
          where: {email: email}
        })
        console.log('user ==>>', user);

        const mails = await sendMails(user[0]); //Send otp in mail;
        console.log('mails ==>>', mails);

        return resolve();
      } catch (error) {
        console.log('sendotp service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  forgot_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          email
        } = body

        // Fetch email from database:
        const user = await User.findAll({
          attributes: ['email'],
          where: {email: email}
        })
        console.log('user ==>>', user);

        // Check email is exist in database or not:
        if(user.length == 0) {
          throw {statusCode:200, message:'Email does not exist!'}
        }

        let otp = await generateOTP(4); //Generate otp;
        console.log('otp ==>>', otp);

        // Update otp and is_verified in users table:
        await User.update({otp: otp, is_verified: 0}, {
          where: {email: email}
        })

        await sendMails({email, otp});  //Send otp in mail;
        return resolve();

      } catch (error) {
        console.log('forgot_password service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  verifyotp(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          email, otp
        } = body

        // Fetch otp from users table:
        const user = await User.findAll({
          attributes: ['otp', 'is_verified'],
          where: {email: email}
        })
        console.log('user ==>>', user);

        // Check user's otp is correct or not:
        if(user[0].otp != otp) {
          throw {statusCode:200, message:'OTP is incorrect!'}
        }

        if(user[0].is_verified == 1) {
          throw {statusCode:200, message:'Please generate otp!'}
        }

        // Update is_verified in users table:
        await User.update({is_verified: 1}, {
          where: {email: email}
        });
        return resolve({is_verified: 1});
      } catch (error) {
        console.log('verifyotp service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  new_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          email, new_password
        } = body

        // Fetch user data from users table:
        const user = await User.findOne({
          attributes: [
            'user_type', 'name', 'email', 'country_code', 'mobile', 'gender', 'is_active', 'is_verified', 'createdAt', 'updatedAt'
          ],
          where: {email: email}
        });
        console.log('user ==>>', user);

        const hash = await encryptedPassword(new_password); //Encrypt new password;
       
        // Update new password in users table:
        await User.update({password: hash}, {
          where: {email: email}
        });

        return resolve({
          userData: {
            user_type: user.user_type,
            name: user.name,
            email: user.email,
            country_code:user.country_code,
            mobile: user.mobile,
            gender: user.gender,
            is_active: user.is_active,
            is_verified: user.is_verified,
            createdAt: user.createdAt,
            updatedAt: user.updatedAt
          }
        });
      } catch (error) {
        console.log('new_password service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  reset_password(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          old_password, new_password, user_id
        } = body

        let hash = await encryptedPassword(new_password); //Encrypt new password;

        // Fetch password from users table:
        const user = await User.findAll({
          attributes: ['password'],
          where: {_id: user_id}
        });

        // Check old password is correct or not:
        if(await decryptedPassword(old_password, user[0].password) == false) {
          throw {statusCode:200, message:'Incorrect old password!'}
        }

        // Update new password in users table:
        await User.update({password: hash}, {
          where: {_id: user_id}
        });
        
        return resolve();
      } catch (error) {
        console.log('reset_password service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_category(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          name
        } = body

        // Salon category details insert into salon_category table:
        let userData = await Salon_category.create(body)
        console.log('userData ==>>', userData);

        return resolve({
          userData: userData
        });
      } catch (error) {
        console.log('add_category service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  get_category(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {} = body
        
        // Fetch salon categories from salon_category table:
        let salonCategory = await Salon_category.findAll({
          attributes: ['_id', 'name'],
          where: { is_active: 1}
        });
        console.log('salonCategory ==>>', salonCategory);
          
        return resolve({userData: salonCategory});
      } catch (error) {
        console.log('get_category service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          name, category_id, price, time, description, salon_id
        } = body
        let {token} = headers

        // Salon service details insert into salon_service table:
        let salonService = await Salon_service.create(body);
        console.log('salonService ==>>', salonService);
        
        return resolve({
          token: token,
          userData: salonService
        });
      } catch (error) {
        console.log('add_service service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  my_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, page, limit } = body;
        let { token } = headers;

        let paginate = await pagination(page, limit);

        // Fetch salon services from salon_service table: 
        let {count, rows} = await Salon_service.findAndCountAll({
          attributes: [
            '_id', 'salon_id', 'category_id', 'name', 'price', 'time', 'description', 'createdAt', 'updatedAt'
          ],
          where: { [Op.and] : [{salon_id: salon_id}, {is_deleted: 0}] },
          order: [ ['createdAt', 'DESC'] ],
          limit: paginate.limit,
          offset: paginate.offset,
          group: ['Salon_service._id']
        });

        let is_last_page = await checkLastPage(count.length, paginate.limit, page);
        return resolve({
          token: token,
          totalData: count.length,
          is_last_page: is_last_page,
          userData: rows
        });
       
      } catch (error) {
        console.log('my_service service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  edit_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          _id, name, category_id, price, time, description
        } = body
        delete body._id;

        // Update service details in salon_service table:
        await Salon_service.update(body, {
          where: {_id: _id}
        });

        // Fetch service details from salon_service table:
        let userData = await Salon_service.findOne({
          attributes: [
            '_id', 'salon_id', 'category_id', 'name', 'price', 'time', 'description'
          ],
          where: {_id: _id}
        });
        console.log('userData ==>>', userData);

        return resolve({ userData: userData });
      } catch (error) {
        console.log('edit_service service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  dlt_service(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { _id } = body

        // Delete service from salon_service table:
        await Salon_service.update({is_deleted: 1}, {
          where: {_id: _id}
        });

        return resolve();
      } catch (error) {
        console.log('dlt_service service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          salon_id, name
        } = body
        let {token} = headers

        // Salon staff details insert into salon_staff table:
        let userData = await Salon_staff.create(body)
        console.log('userData ==>>', userData);

        return resolve({
          token: token,
          userData: userData
        });
      } catch (error) {
        console.log('add_barber service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  my_staff(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, page, limit } = body;
        let { token } = headers;

        let paginate = await pagination(page, limit);

        // Fetch staff details from salon_staff table:
        let {count, rows} = await Salon_staff.findAndCountAll({
          attributes: [
            '_id', 'name', 'available', 'createdAt', 'updatedAt'
          ],
          where: { [Op.and] : [{salon_id: salon_id}, {is_deleted: 0}] },
          limit: paginate.limit,
          offset: paginate.offset,
          order: [ ['createdAt', 'DESC'] ],
          group: ['Salon_staff._id']
        });

        let is_last_page = await checkLastPage(count.length, paginate.limit, page);
        return resolve({
          token: token,
          totalData: count.length,
          is_last_page: is_last_page,
          userData: rows
        });

      } catch (error) {
        console.log('my_staff service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  edit_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          _id, name
        } = body
        let {token} = headers

        const barber_id = await Salon_staff.findOne({
          attributes: ['_id'],
          where: {_id}
        });

        if(barber_id == null) {throw{statusCode:200, message:'This barber is not exist'}}

        // Update barber details in salon_staff table:
        await Salon_staff.update({name}, {
          where: {_id: _id}
        });

        // Fetch barber details from salon_staff table:
        let userData = await Salon_staff.findOne({
          attributes: ['_id', 'name', 'available', 'createdAt', 'updatedAt'],
          where: {_id: _id}
        });
        console.log('userData ==>>', userData);

        return resolve({
          token: token,
          userData: userData
        });
      } catch (error) {
        console.log('edit_barber service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  delete_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { _id } = body

        const barber_id = await Salon_staff.findOne({
          attributes: ['_id'],
          where: {_id}
        });

        if(barber_id == null) { throw{statusCode:200, message:'This barber is not exist'}}

        // Delete barber from salon_staff table:
        await Salon_staff.update({is_deleted: 1}, {
          where: {_id: _id}
        });

        return resolve();
      } catch (error) {
        console.log('delete_barber service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  barber_availability(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { _id } = body

        // Fetch available from Salon_staff table:
        let available = await Salon_staff.findOne({
          attributes: ['available'],
          where: {_id: _id}
        })
        
        if(available.available == 0) {
          // Update available = 1, if 0 in salon_staff table:
          await Salon_staff.update({available: 1}, {
            where: {_id: _id}
          }); 
        }
        else {
          // Update available = 0, if 1 in salon_staff table: 
          await Salon_staff.update({available: 0}, {
            where: {_id: _id}
          });
        }

        return resolve();
      } catch (error) {
        console.log('barber_availability service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  refresh_token(headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          token, refresh_token
        } = headers

        if(!token) { throw {statusCode:200, message:'Token is require!'} }
        else if(!refresh_token) { throw {statusCode:200, message:'Refresh token is require!'} };

        if(refresh_token != defaultRefreshToken) {
          throw {statusCode:200, message:'Refresh_token is incorrect!'}; //Throw error if refresh token is incorrect;
        }

        let decode = jwt.decode(token); //Decode auth token;
        let user_id = decode.user_id; //Get user id from decode;

        let new_token = await getJwtToken(user_id); //Create new auth token;
        console.log('new_token ==>>', new_token);

        return resolve({
          token: new_token
        });

      } catch (error) {
        console.log('refresh_token service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  list_salon_business_hours_weekly_schedule(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, schedule_type } = body;
        let salon_time = [];

        if(schedule_type == 2) {
          throw {statusCode:200, message:'List of weekly schedule is only for schedule type 1'};
        }

        for(let i=1; i<=7; i++) {
          // Fetch salon weekly schedule details from salon_business_hours table:
          let checkDetails = await Salon_business_hours.findOne({
            attributes: [
              '_id', 'salon_id', 'day', 'working_start_time',
              'working_end_time', 'break_start_time', 'break_end_time', 'available'
            ],
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {day: i}] }
          });

          if(checkDetails != null) {
            salon_time.push(checkDetails);
          }
          else {
            let time_details = {
              _id: "",
              salon_id: salon_id,
              day: i,
              working_start_time: "",
              working_end_time: "",
              break_start_time: "",
              break_end_time: "",
              available: 0
            }
            salon_time.push(time_details);
          }
        }
        return resolve({ userData: salon_time });
      } catch (error) {
        console.log('list_salon_business_hours_weekly_schedule service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_salon_business_hours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          salon_id, schedule_type, day, date, working_start_time, working_end_time, break_start_time, break_end_time, available
        } = body;

        if(schedule_type == 2) {
          // If salon business time details are not exist in table then insert into salon_business_hours table else return find data:
          let [userData, created] = await Salon_business_hours.findOrCreate({
            where: { [Op.and] : [{salon_id: salon_id}, {date: date}] },
            defaults: body
          });
          console.log('userData ==>>', userData);
          console.log('created ==>>', created);

          if(created == false) {
            // Update salon daily business time details in salon_business_hours table:
            await Salon_business_hours.update({
              working_start_time, working_end_time, break_start_time, break_end_time, available
            },{
              where: { [Op.and] : [{salon_id: salon_id}, {date: date}]}
            });

            // Fetch salon daily business time details from salon_business_hours table:
            let userData = await Salon_business_hours.findOne({
              attributes: [
                '_id', 'salon_id', 'schedule_type', 'day', 'date', 'working_start_time', 'working_end_time',
                'break_start_time', 'break_end_time', 'available', 'createdAt', 'updatedAt'
              ],
              where: { [Op.and] : [{salon_id: salon_id}, {date: date}]}
            });

            console.log('userData ==>>', userData);
            return resolve({ userData: userData});
          }
          return resolve({ userData: userData });
        }

        // If salon business time details are not exist in table then insert into salon_business_hours table else return find data:
        let [userData, created] = await Salon_business_hours.findOrCreate({
          where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {day: day}] },
          defaults: body
        });
        console.log('userData ==>>', userData);
        console.log('created ==>>', created);

        if(created == false) {
          // Update salon weekly business time details in salon_business_hours table:
          await Salon_business_hours.update({
            working_start_time, working_end_time, break_start_time, break_end_time, available 
          },{
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {day: day}] }
          });

          // Fetch salon weekly business time details from salon_business_hours table:
          let userData = await Salon_business_hours.findOne({
            attributes: [
              '_id', 'salon_id', 'day', 'working_start_time', 'working_end_time',
              'break_start_time', 'break_end_time', 'available', 'createdAt', 'updatedAt'
            ],
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {day: day}] }
          });

          console.log('userData ==>>', userData);
          return resolve({ userData: userData });
        }
        return resolve({ userData: userData });
      } 
      catch (error) {
        console.log('add_salon_business_hours service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  list_salon_business_hours_daily_schedule(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, schedule_type, day, date } = body;

        // Fetch salon daily schedule details from salon_business_hours table:
        let checkDetails = await Salon_business_hours.findOne({
          attributes: [
            '_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time', 'available'
          ],
          where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {date: date}] }
        });

        if(checkDetails == null) {
          // Fetch salon weekly schedule details from salon_business_hours table:
          let userData = await Salon_business_hours.findOne({
            attributes: [
              '_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time', 'available'
            ],
            where: { [Op.and] : [{schedule_type: 1}, {salon_id: salon_id}, {day: day}] }
          });

          if(userData == null) {
            let time_details = {
              _id: "",
              working_start_time: "",
              working_end_time: "",
              break_start_time: "",
              break_end_time: "",
              available: 0
            }
            return resolve({userData: time_details});
          }
          return resolve({ userData: userData });
        }
        return resolve({ userData: checkDetails });
      } catch (error) {
        console.log('list_salon_business_hours_daily_schedule service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_business_hours_availability(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, day} = body

        // Fetch available from Salon_business_hours table:
        let available = await Salon_business_hours.findOne({
          attributes: ['_id', 'available'],
          where: {salon_id: salon_id, schedule_type: 1, day: day}
        })

        if(available == null) {
          throw {statusCode:200, message:'Please add working time'} //If data is not exists salon_business_hours table;
        }
        
        if(available.available == 0) {
          // Update available = 1, if 0 in salon_business_hours table:
          await Salon_business_hours.update({available: 1}, {
            where: {_id: available._id}
          }); 
        }
        else {
          // Update available = 0, if 1 in salon_business_hours table: 
          await Salon_business_hours.update({available: 0}, {
            where: {_id: available._id}
          });
        }

        return resolve();
      } catch (error) {
        console.log('salon_business_hours_availability service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  list_barber_business_hours_weekly_schedule(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, barber_id, schedule_type } = body;
        let salon_time = [];

        if(schedule_type == 2) {
          throw {statusCode:200, message:'List of weekly schedule is only for schedule type 1'};
        }

        for(let i=1; i<=7; i++) {
          // Fetch barber weekly schedule details from barber_business_hours table:
          let checkDetails = await Barber_business_hours.findOne({
            attributes: [
              '_id', 'salon_id', 'barber_id', 'day', 'working_start_time',
              'working_end_time', 'break_start_time', 'break_end_time', 'available'
            ],
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {barber_id: barber_id}, {day: i}] }
          });

          if(checkDetails != null) {
            salon_time.push(checkDetails);
          }
          else {
            let time_details = {
              _id: "",
              salon_id: salon_id,
              barber_id: barber_id,
              day: i,
              working_start_time: "",
              working_end_time: "",
              break_start_time: "",
              break_end_time: "",
              available: 0
            }
            salon_time.push(time_details);
          }
        }
        return resolve({ userData: salon_time });
      } catch (error) {
        console.log('list_barber_business_hours_weekly_schedule service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_barber_business_hours(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          salon_id, barber_id, schedule_type, day, date, working_start_time,
          working_end_time, break_start_time, break_end_time, available
        } = body;

        if(schedule_type == 2) {
          // If barber business time details are not exist in table then insert into barber_business_hours table else return find data:
          let [userData, created] = await Barber_business_hours.findOrCreate({
            where: { [Op.and] : [{salon_id: salon_id}, {barber_id: barber_id}, {date: date}] },
            defaults: body
          });

          if(created == false) {
            // Update barber daily business time details in barber_business_hours table:
            await Barber_business_hours.update({
              working_start_time, working_end_time, break_start_time, break_end_time, available
            },{
              where: { [Op.and] : [{salon_id: salon_id}, {barber_id: barber_id}, {date: date}]}
            });

            // Fetch barber daily business time details from barber_business_hours table:
            let userData = await Barber_business_hours.findOne({
              attributes: [
                '_id', 'salon_id', 'barber_id', 'schedule_type', 'day', 'date', 'working_start_time', 'working_end_time',
                'break_start_time', 'break_end_time', 'available', 'createdAt', 'updatedAt'
              ],
              where: { [Op.and] : [{salon_id: salon_id}, {barber_id: barber_id}, {date: date}]}
            });
            return resolve({ userData: userData});
          }
          return resolve({ userData: userData });
        }

        // If barber business time details are not exist in table then insert into barber_business_hours table else return find data:
        let [userData, created] = await Barber_business_hours.findOrCreate({
          where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {barber_id: barber_id}, {day: day}] },
          defaults: body
        });

        if(created == false) {
          // Update barber weekly business time details in barber_business_hours table:
          await Barber_business_hours.update({
            working_start_time, working_end_time, break_start_time, break_end_time, available 
          },{
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {barber_id: barber_id}, {day: day}] }
          });

          // Fetch barber weekly business time details from barber_business_hours table:
          let userData = await Barber_business_hours.findOne({
            attributes: [
              '_id', 'salon_id', 'barber_id', 'day', 'working_start_time', 'working_end_time',
              'break_start_time', 'break_end_time', 'available', 'createdAt', 'updatedAt'
            ],
            where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {barber_id: barber_id}, {day: day}] }
          });
          return resolve({ userData: userData });
        }
        return resolve({ userData: userData });
      } 
      catch (error) {
        console.log('add_barber_business_hours service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  list_barber_business_hours_daily_schedule(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, barber_id, schedule_type, day, date } = body;

        // Fetch barber daily schedule details from barber_business_hours table:
        let checkDetails = await Barber_business_hours.findOne({
          attributes: [
            '_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time', 'available'
          ],
          where: { [Op.and] : [{schedule_type: schedule_type}, {salon_id: salon_id}, {barber_id: barber_id}, {date: date}] }
        });

        if(checkDetails == null) {
          // Fetch barber weekly schedule details from barber_business_hours table:
          let userData = await Barber_business_hours.findOne({
            attributes: [
              '_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time', 'available'
            ],
            where: { [Op.and] : [{schedule_type: 1}, {salon_id: salon_id}, {barber_id: barber_id}, {day: day}] }
          });

          if(userData == null) {
            let time_details = {
              _id: "",
              working_start_time: "",
              working_end_time: "",
              break_start_time: "",
              break_end_time: "",
              available: 0
            }
            return resolve({userData: time_details});
          }
          return resolve({ userData: userData });
        }
        return resolve({ userData: checkDetails });
      } catch (error) {
        console.log('list_barber_business_hours_daily_schedule service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  barber_business_hours_availability(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, barber_id, day} = body

        // Fetch available from Barber_business_hours table:
        let available = await Barber_business_hours.findOne({
          attributes: ['_id', 'available'],
          where: {salon_id: salon_id, barber_id: barber_id, schedule_type: 1, day: day}
        })
        console.log('available ===>>', available);

        if(available == null) {
          throw {statusCode:200, message:'Please add working time'} //If data is not exists barber_business_hours table;
        }
        
        if(available.available == 0) {
          // Update available = 1, if 0 in Barber_business_hours table:
          await Barber_business_hours.update({available: 1}, {
            where: {_id: available._id}
          }); 
        }
        else {
          // Update available = 0, if 1 in Barber_business_hours table: 
          await Barber_business_hours.update({available: 0}, {
            where: {_id: available._id}
          });
        }

        return resolve();
      } catch (error) {
        console.log('barber_business_hours_availability service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  edit_user_profile(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, name, email, country_code, mobile, gender
        } = body;
        
        // Update user profile details in users table:
        await User.update({name, email, country_code, mobile, gender},{
          where: {_id: user_id}
        });

        // Fetch user profile details from users table:
        let userData = await User.findOne({
          attributes: [
            '_id', 'user_type', 'name', 'email', 'country_code', 'mobile',
            'gender', 'is_active', 'is_verified', 'createdAt', 'updatedAt'],
          where: {_id: user_id}
        });
        console.log('userData ==>>', userData);

        return resolve({
          userData: {
            _id:userData._id,
            user_type: userData.user_type,
            name: userData.name,
            email: userData.email,
            country_code:userData.country_code,
            mobile: userData.mobile,
            gender: userData.gender,
            is_active: userData.is_active,
            is_verified: userData.is_verified,
            createdAt: userData.createdAt,
            updatedAt: userData.updatedAt
          }
        });
      }
      catch(error) {
        console.log('edit_user_profile service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  edit_salon_profile(body, file) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          salon_id, name, country_code, mobile, salon_type, salon_name, address, service_gender, description
        } = body;
       
        if(file == null) {
          // Update salon profile details without image in users table:
          await User.update(
            {name, country_code, mobile, salon_type, salon_name, address, service_gender, description},
            {
              where: {_id: salon_id}
            }
          );
        }
        else {
          let profilePicDirPath = path.join(__dirname + profilePicPath); 
          let profile_pic = await User.findOne({
            attributes: ['profile_pic'],
            where: {_id: salon_id}
          })
          fs.unlinkSync(profilePicDirPath + profile_pic.profile_pic);

          // Update salon profile details with image in users table:
          await User.update(
            {name, country_code, mobile, profile_pic : file.filename, salon_type, salon_name, address, service_gender, description},
            {
              where: {_id: salon_id}
            }
          );
        }

        // Fetch salon profile details from users table:
        let userData = await User.findOne({
          attributes: ['_id', 'user_type', 'name', 'email', 'country_code', 'mobile', 'profile_pic', 'salon_type', 'salon_name',
                        'address', 'service_gender', 'description', 'is_active', 'is_verified', 'createdAt', 'updatedAt'],
          where: {_id: salon_id}
        });

        console.log('userData ==>>', userData);
        let image_url =  profile_pic(userData.profile_pic);
        return resolve({
          userData: {
            _id:userData._id,
            user_type: userData.user_type,
            name: userData.name,
            email: userData.email,
            country_code:userData.country_code,
            mobile: userData.mobile,
            salon_type: userData.salon_type,
            salon_name: userData.salon_name,
            profile_pic: image_url,
            address: userData.address,
            service_gender: userData.service_gender,
            description: userData.description,
            is_active: userData.is_active,
            is_verified: userData.is_verified,
            createdAt: userData.createdAt,
            updatedAt: userData.updatedAt
          }
        });
      }
      catch(error) {
        console.log('edit_salon_profile service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  contact_us(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, name, email, country_code, mobile, subject, message
        } = body;

        // User's contact us details insert into database:
        let userData = await Contact_us.create(body);
        console.log('userData ==>>', userData);

        await sendMails(body);
        return resolve();
      }
      catch(error) {
        console.log('contact_us service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  referral_code(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id } = body;

        // Fetch referral code from users table:
        let referral_code = await User.findOne({
          attributes: ['referral_code'],
          where: {_id: user_id}
        });
        return resolve(referral_code);
      }
      catch(error) {
        console.log('referral_code service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  search_salon(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, search_type, address, salon_or_barber_name, salon_type, latitude,
          longitude, distance, service_gender, min_price, max_price, review_star, page, limit
        } = body;
        var include_price, having;
        let paginate = await pagination(page, limit);

        // If price is given in filter then give this conditions from salon_service table:
        if(min_price || max_price) {
          include_price = {
            model: Salon_service,
            as: 'salon_service',
            attributes: [],
            where: { 
              is_deleted : 0,
              price: { [Op.between] : [min_price, max_price] }
            }
          }
        } 
        else {
          include_price = {
            model: Salon_service,
            as: 'salon_service',
            attributes: []
          }
        }

        if(review_star) {
          having = where(fn('ROUND', fn('AVG', col('avg_review.avg_review'))), {
            [Op.in]: [review_star]
          })
        }
        else {
          having = ''
        }

        if(search_type == 1) {
          // Fetch salon_ids from users table using with scope method:
          let salonSalonId = await User.scope({
            method: ['distance', latitude, longitude, distance]
          })
          .findAll({
            attributes: ['_id'],
            where: {
              user_type: 2, salon_type: salon_type, is_deleted: 0,
              [Op.or] : [{service_gender: service_gender}, {service_gender: 3}],
            },
            order: col('distance')
          })
          console.log('salonSalonId ==>>', salonSalonId);

          // Array without duplicate elements;
          var salon_id = salonSalonId.map(a => a._id)
          console.log('salon_id ==>>', salon_id);
        }

        else {
          // Fetch salon_id from salon_staff table with use of sequelize belongsTo in User model:
          let barberSalonId = await Salon_staff.findAll({
            include: [{
              model: User,
              as: 'users',
              attributes: [],
              where: {
                salon_type: salon_type,
                [Op.or] : [{service_gender: service_gender}, {service_gender: 3}]
              },
              required: false
            }],
            attributes: [['salon_id', '_id']],
            where: {name:  {
              [Op.iLike]: `%${salon_or_barber_name}%`  //Ilike: case insensitive operator;
            }, is_deleted: 0},
            group: ['Salon_staff.salon_id']
          });

          // Fetch salon_ids from users table:
          let salonSalonId = await User.findAll({
            attributes: ['_id'],
            where: {
              user_type: 2,
              salon_type: salon_type,
              is_deleted: 0,
              [Op.or] : [{service_gender: service_gender}, {service_gender: 3}],
              salon_name: {
                [Op.iLike]:  `%${salon_or_barber_name}%` //Ilike: case insensitive operator;
              }
            }
          });

          // Merge two array without duplicate elements;
          var salon_id = [...new Set([...(barberSalonId.map(a => a._id)), ...(salonSalonId.map(a => a._id))])]
          console.log('salon_id ==>>', salon_id);
        }

        // Fetch salon details from users table with nested includes:
        let {count, rows} = await User.findAndCountAll({
          include: [include_price,
            {
              model: Salon_review,
              as: 'avg_review',
              attributes: [],
              required: false
            },{
              model: Favorite_salon,
              as: 'is_fav',
              attributes: [],
              where: { user_id: user_id},
              required: false
            }
          ],
          attributes: [
            [fn('concat', profilePicDir, '/', col('profile_pic')), 'profile_pic'], //Concat url with profile_pic;
            [cast(fn('ROUND', fn('AVG', col('avg_review.avg_review')), 2), 'double precision'), 'avg_review'],
            '_id', 'salon_type', 'salon_name', 'address', 'is_fav.is_fav'
          ],
          where: {_id: { [Op.in]: salon_id }},
          having: having,
          raw: true,
          order: [ ['_id', 'ASC'] ],
          group: ['User._id', 'is_fav.is_fav'],
          limit: paginate.limit,
          offset: paginate.offset,
          subQuery: false
        });
        let salon_details = rows.filter(Boolean); //Remove null value;

        let is_last_page = await checkLastPage(count.length, paginate.limit, page);
        return resolve({
          totalData: count.length,
          is_last_page: is_last_page,
          userData: salon_details
        })
      }
      catch(error) {
        console.log('search_salon service catch error ===>>', error);
        return reject(error);
      }
    });
  }

  like_dislike_salon(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id } = body;

        // If fav salon details are not exist in table then insert into favorite_salon table else return find data:
        let [like_or_dislike, created] = await Favorite_salon.findOrCreate({
          attributes: ['_id', 'is_fav'],
          where: {user_id: user_id, salon_id: salon_id},
          defaults: {user_id, salon_id, is_fav: 1}
        });

        if(created == false) {
          // Delete fav salon detail from favorite_salon table:
          let deleted = await Favorite_salon.destroy({
            where: {_id: like_or_dislike._id}
          });
          return resolve(messages.removeFavSalon);
        }
        return resolve(messages.addFavSalon);
      }
      catch(error) {
        console.log('like_dislike_salon service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  my_favorite_salons(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, limit} = body;

        let paginate = await pagination(page, limit);

        // Fetch fav salon details from favorite_salon table with sequelize belongsTo User model:
        let {count, rows} = await Favorite_salon.findAndCountAll({
          include: [{
            model: User,
            as: 'users',
            include: [{
              model: Salon_review,
              as: 'avg_review',
              attributes: []
            }],
            attributes: []
          }],
          attributes: ['is_fav', [fn('concat', profilePicDir, '/', col('users.profile_pic')), 'profile_pic'],
            [cast(fn('ROUND', fn('AVG', col('users->avg_review.avg_review')), 2), 'double precision'), 'avg_review'],
            'users._id', 'users.salon_type', 'users.salon_name', 'users.address'
          ],
          where: {user_id: user_id},
          raw: true,
          order: [['salon_id', 'ASC']],
          group: ['Favorite_salon.is_fav', 'Favorite_salon.salon_id', 'users._id'],
          limit: paginate.limit,
          offset: paginate.offset
        });

        let is_last_page = await checkLastPage(count.length, paginate.limit, page);
        return resolve({
          totalData: count.length,
          is_last_page: is_last_page,
          userData: rows
        });
      }
      catch(error) {
        console.log('my_favorite_salons service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  write_review(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, salon_id, price, value, quality, friendliness, cleanliness, description
        } = body;

        let avg_review = (price + value + quality + friendliness + cleanliness)/5

        let [salon_review, created] = await Salon_review.findOrCreate({
          attributes: ['_id'],
          where: {user_id: user_id, salon_id: salon_id},
          defaults: {
            user_id, salon_id, price, value, quality, friendliness, cleanliness, description, avg_review: avg_review
          }
        });

        if(created == false) {
          await Salon_review.update({
            price, value, quality, friendliness, cleanliness, description, avg_review: avg_review
          },{
            where: {user_id: user_id, salon_id: salon_id}
          })

          let salon_review = await Salon_review.findOne({
            attributes: [
              '_id', 'user_id', 'salon_id', 'price', 'value', 'quality', 'friendliness', 'cleanliness', 'avg_review', 'description'
            ],
            where: {user_id: user_id, salon_id: salon_id}
          });

          return resolve({userData: salon_review});
        }
        return resolve({userData: salon_review});
      }
      catch(error) {
        console.log('write_review service catch error ===>>', error);
        return reject(error);
      }
    });
  }

  list_review(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, salon_id, page, limit
        } = body;
        var is_fav;

        const userType = await User.findOne({
          attributes: ['user_type'],
          where: {_id: user_id}
        });
        let paginate = await pagination(page, limit);

        if(userType.user_type == 1) {
          let avg_review  = await Salon_review.findOne({
            attributes: [[cast(fn('ROUND', fn('AVG', col('avg_review')), 2), 'double precision'), 'avg_review']],
            where: {salon_id}
          });

          var {count, rows} = await User.findAndCountAll({
            include: [{
              model: Salon_review,
              as: 'salon_reviews',
              include: {
                model: User,
                as: 'user_details',
                attributes: [],
                required: true
              },
              attributes: [
                'user_id', 'salon_id', 'price', 'value', 'quality', 'friendliness', 'cleanliness', 'description',
                [fn('to_char', col('salon_reviews.createdAt'), "DD Mon YYYY"), 'createdAt'],
                [literal('"salon_reviews->user_details"."name"'), 'name']
              ]
            },{
              model: Favorite_salon,
              as: 'is_fav',
              attributes: ['is_fav'],
              where: {user_id: user_id},
              required: false
            }],
            attributes: [[fn('concat', profilePicDir, '/', col('User.profile_pic')), 'profile_pic'],
              '_id', 'salon_type', 'salon_name', 'service_gender'
            ],
            where: {_id: salon_id},
            group: ['User._id', 'is_fav._id', 'salon_reviews._id', 'salon_reviews->user_details.name'],
            limit:  paginate.limit,
            offset: paginate.offset,
            subQuery: false
          });

          if(rows[0].is_fav == null) { is_fav = null}
          else { is_fav = rows[0].is_fav.is_fav}

          if(count.length == 1 && count[0]._id == null) {
            return resolve({
              totalData: 0,
              is_last_page: true,
              userData: {
                avg_review: avg_review.avg_review,
                total_review: 0,
                profile_pic : rows[0].profile_pic,
                salon_type: rows[0].salon_type,
                salon_name: rows[0].salon_name,
                service_gender: rows[0].service_gender,
                is_fav: is_fav,
                salon_reviews: rows[0].salon_reviews
              }
            });
          }

          var userData = {
            avg_review: avg_review.avg_review,
            total_review: count.length,
            profile_pic : rows[0].profile_pic,
            salon_type: rows[0].salon_type,
            salon_name: rows[0].salon_name,
            service_gender: rows[0].service_gender,
            is_fav: is_fav,
            salon_reviews: rows[0].salon_reviews
          }
        }
        else {
          let avg_review  = await Salon_review.findOne({
            attributes: [[cast(fn('ROUND', fn('AVG', col('avg_review')), 2), 'double precision'), 'avg_review']],
            where: {salon_id: user_id}
          });

          var {count, rows} = await User.findAndCountAll({
            include:[
              {
                model: Salon_review,
                as: 'salon_reviews',
                include: {
                  model: User,
                  as: 'user_details',
                  attributes: []
                },
                attributes: [
                  'user_id', 'salon_id', 'price', 'value', 'quality', 'friendliness', 'cleanliness', 'description',
                  [fn('to_char', col('salon_reviews.createdAt'), "DD Mon YYYY"), 'createdAt'],
                  [literal('"salon_reviews->user_details"."name"'), 'name']
                ]
              }
            ],
            attributes: [[fn('concat', profilePicDir, '/', col('User.profile_pic')), 'profile_pic'],
              '_id', 'salon_type', 'salon_name', 'service_gender'
            ],
            where: {_id: user_id},
            group: ['User._id', 'salon_reviews._id', 'salon_reviews->user_details.name'],
            // limit:  paginate.limit,
            // offset: paginate.offset,
            // subQuery: false
          });

          if(count.length == 1 && count[0]._id == null) {
            return resolve({
              totalData: 0,
              is_last_page: true,
              userData: {
                avg_review: avg_review.avg_review,
                total_review: 0,
                profile_pic : rows[0].profile_pic,
                salon_type: rows[0].salon_type,
                salon_name: rows[0].salon_name,
                service_gender: rows[0].service_gender,
                salon_reviews: rows[0].salon_reviews
              }
            });
          }

          var userData = {
            avg_review: avg_review.avg_review,
            total_review:count.length,
            profile_pic : rows[0].profile_pic,
            salon_type: rows[0].salon_type,
            salon_name: rows[0].salon_name,
            service_gender: rows[0].service_gender,
            salon_reviews: rows[0].salon_reviews
          }
        }

        let is_last_page = await checkLastPage(count.length, paginate.limit, page);
        return resolve({
          totalData: count.length,
          is_last_page: is_last_page,
          userData
        });
      }
      catch(error) {
        console.log('list_review service catch error ===>>', error);
        return reject(error);
      }
    });
  }

  salon_profile_details(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id } = body;

        let {count, rows} = await User.findAndCountAll({
          include: [{
            model: Salon_review,
            as: 'avg_review',
            attributes: []
          }, {
            model: Favorite_salon,
            as: 'is_fav',
            attributes: [],
            where: {user_id},
            required: false
          }],
          attributes: [[fn('concat', profilePicDir, '/', col('profile_pic')), 'profile_pic'],
            [cast(fn('ROUND', fn('AVG', col('avg_review.avg_review')), 2), 'DOUBLE PRECISION'), 'avg_review'],
            '_id', 'email', 'mobile', 'salon_type', 'salon_name', 'service_gender', 'address', 'description', 'is_fav.is_fav'
          ],
          where: {_id: salon_id},
          raw: true,
          group: ['User._id', 'is_fav.is_fav', 'avg_review.salon_id']
        });

        let salon_time = await Salon_business_hours.findAll({
          attributes: ['day', 'working_start_time', 'working_end_time'],
          where: {salon_id: salon_id, schedule_type: 1},
          order: [['day', 'ASC']]
        });

        let opening_hours = [];
        for(let i=1; i<=7; i++) {
          if(salon_time.length == 0) {
            let time_details = {
              day: i,
              working_start_time: '',
              working_end_time: '',
              is_closed: 1
            }
            opening_hours.push(time_details);
          }
          else if(salon_time[0].day != i) {
            let time_details = {
              day: i,
              working_start_time: '',
              working_end_time: '',
              is_closed: 1
            }
            opening_hours.push(time_details);
          }
          else {
            salon_time[0].dataValues.is_closed = 0;
            opening_hours.push(salon_time[0]);
            salon_time.shift();
          }
        }

        let salon_gallery = await Salon_images_videos.findAll({
          attributes: ['is_image_video', [fn('concat', imageVideoDir, '/', col('images_videos')), 'images_videos']],
          where: {salon_id: salon_id}
        });

        if(count.length == 1 && count[0].salon_id == null) {
          return resolve({userData: {
            salon_id: rows[0]._id,
            total_review: 0,
            avg_review: rows[0].avg_review,
            is_fav: rows[0].is_fav,
            profile_pic : rows[0].profile_pic,
            salon_email: rows[0].email,
            salon_mobile: rows[0].mobile,
            salon_type: rows[0].salon_type,
            salon_name: rows[0].salon_name,
            service_gender: rows[0].service_gender,
            address: rows[0].address,
            description: rows[0].description,
            opening_hours: opening_hours,
            gallery: salon_gallery
          }});
        }

        var userData = {
          salon_id: rows[0]._id,
          total_review: count[0].count,
          avg_review: rows[0].avg_review,
          is_fav: rows[0].is_fav,
          profile_pic : rows[0].profile_pic,
          salon_email: rows[0].email,
          salon_mobile: rows[0].mobile,
          salon_type: rows[0].salon_type,
          salon_name: rows[0].salon_name,
          service_gender: rows[0].service_gender,
          address: rows[0].address,
          description: rows[0].description,
          opening_hours: opening_hours,
          gallery: salon_gallery
        }
        return resolve({userData});
      }
      catch(error) {
        console.log('salon_profile_details service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_image_video(body, files) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, is_image_video } = body;
        let gallery = [];

        for(let i=0; i<files.length; i++) {
          body.images_videos = files[i].filename

          let image_video = await Salon_images_videos.create(body)
          gallery.push(image_video)
          console.log('image_video ===>>', image_video);
        }
        console.log('gallery ===>>', gallery);
        return resolve();
      }
      catch(error) {
        console.log('add_image_video service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  get_image_video(body, files) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id, page, limit } = body;
        let paginate = await pagination(page, limit);
        let id;

        if(!salon_id) { id = user_id }
        else {id = salon_id}

        let {count, rows} = await Salon_images_videos.findAndCountAll({
          attributes: ['_id', 'is_image_video',
            [fn('concat', imageVideoDir, '/', col('images_videos')), 'images_videos']
          ],
          where: { salon_id: id },
          order: [ ['createdAt', 'DESC'] ],
          limit: paginate.limit,
          offset: paginate.offset
        });
        
        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData: rows
        });
      }
      catch(error) {
        console.log('get_image_video service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  delete_image_video(body, files) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, _id } = body;
        let imageVideoDirPath = path.join(__dirname + imageVideoPath);

        let image_video = await Salon_images_videos.findAll({
          attributes: ['images_videos'],
          where: {_id: {[Op.in]: _id}}
        });

        await Salon_images_videos.destroy({
          where: {_id: {[Op.in]: _id}}
        });

        if(image_video.length >= 2) {
          image_video.map((filename) => {
            fs.unlinkSync(imageVideoDirPath + filename.images_videos);
          });
        }
        else {
          fs.unlinkSync(imageVideoDirPath + image_video[0].images_videos);
        }
        return resolve();
      }
      catch(error) {
        console.log('delete_image_video service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_profile_gallery(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id } = body;

        let {count, rows} = await User.findAndCountAll({
          include: [{
            model: Salon_review,
            as: 'avg_review',
            attributes: [],
            required: false
          }, {
            model: Favorite_salon,
            as: 'is_fav',
            attributes: [],
            where: {user_id},
            required: false
          }],
          attributes: [[fn('concat', profilePicDir, '/', col('profile_pic')), 'profile_pic'],
            [fn('ROUND', fn('AVG', col('avg_review.avg_review')), 2), 'avg_review'],
            '_id', 'salon_type', 'salon_name', 'service_gender', 'address', 'is_fav.is_fav'
          ],
          where: {_id: salon_id},
          raw: true,
          group: ['User._id', 'is_fav.is_fav']
        })

        let salon_gallery = await Salon_images_videos.findAll({
          attributes: ['is_image_video', [fn('concat', imageVideoDir, '/', col('images_videos')), 'images_videos']],
          where: {salon_id: salon_id}
        });

        var userData = {
          total_review: count[0].count,
          avg_review: rows[0].avg_review,
          is_fav: rows[0].is_fav,
          profile_pic : rows[0].profile_pic,
          salon_type: rows[0].salon_type,
          salon_name: rows[0].salon_name,
          address: rows[0].address,
          gallery: salon_gallery
        }
        return resolve({userData});
      }
      catch(error) {
        console.log('salon_profile_gallery service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  order_services(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, category_id } = body;

        // Fetch salon services from salon_service table: 
        let userData = await Salon_service.findAll({
          attributes: [
            '_id', 'salon_id', 'category_id', 'name', 'price', 'time', 'description'
          ],
          where: {salon_id, category_id, is_deleted: 0},
          order: [ ['createdAt', 'DESC'] ]
        });
        console.log('userData ==>>', userData);

        return resolve({ userData });
       
      } catch (error) {
        console.log('order_services service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  order_barber(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, page, limit } = body;
        let paginate = await pagination(page, limit);

        // Fetch staff details from salon_staff table:
        let {count, rows} = await Salon_staff.findAndCountAll({
          attributes: [
            '_id', 'name'
          ],
          where: { salon_id, is_deleted: 0, available:1 },
          order: [ ['createdAt', 'DESC'] ],
          limit: paginate.limit,
          offset: paginate.offset
        });

        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData: rows
        });

      } catch (error) {
        console.log('order_barber service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  order_date_time(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, salon_id, barber_id, date, day } = body;
        let id;
        console.log('body ==>>..', body);

        if(!date) {
          let date = new Date();
          var currentDate = moment(date).format("YYYY-MM-DD");
          body.date = currentDate;
          var currentDay = moment(date).format("d");
          body.day = currentDay;
        }

        if(salon_id) { id = salon_id; }
        else { id = user_id }
        
        let checkDate = await Barber_business_hours.findOne({
          attributes: ['_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time'],
          where: {salon_id: id, barber_id, available:1, date: body.date}
        });
        console.log('checkDate ==>>', checkDate);

        if(checkDate == null) {
          var userData = await Salon_business_hours.findOne({
            include : [{
              model: User,
              as: 'users',
              include:{
                model: Barber_business_hours,
                as: 'barber_time',
                attributes: [],
                where: {barber_id, schedule_type:1, available:1, day: body.day},
                required: false
              },
              attributes: [
                [literal('"users->barber_time"."_id"'), '_id'],
                [literal('"users->barber_time"."working_start_time"'), 'working_start_time'],
                [literal('"users->barber_time"."working_end_time"'), 'working_end_time'],
                [literal('"users->barber_time"."break_start_time"'), 'break_start_time'],
                [literal('"users->barber_time"."break_end_time"'), 'break_end_time']
              ]
            }],
            attributes: ['_id', 'working_start_time', 'working_end_time', 'break_start_time', 'break_end_time'],
            where: {salon_id: id, schedule_type:1, available:1, day: body.day}
          });
          console.log('userData ==>>', userData);

          if(userData == null) {
            throw {statusCode:200, message:'Barber time detail is not available for this day'}
          }
          if(userData.users == null) {
            let timeSlots = time_slots(userData.working_start_time, userData.working_end_time,
              userData.break_start_time, userData.break_end_time);
            return resolve({ userData : timeSlots })
          }
          let timeSlots = time_slots(userData.users.dataValues.working_start_time, userData.users.dataValues.working_end_time,
            userData.users.dataValues.break_start_time, userData.users.dataValues.break_end_time);
          return resolve({ userData: timeSlots });
        }
        let timeSlots = time_slots(checkDate.working_start_time, checkDate.working_end_time,
          checkDate.break_start_time, checkDate.break_end_time);
        return resolve({ userData: timeSlots });

      } catch (error) {
        console.log('order_date_time service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_order(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let {
          user_id, salon_id, service_id, barber_id, order_date, order_time, is_offer,
          total_price, walk_in_user_name, walk_in_user_cc, walk_in_user_number
        } = body;
        let notificationType = 4;
        let service_details = [];

        if(salon_id) {
          // Insert salon order details into salon_order_details table:
          let orderData = await Salon_order_details.create({
            user_id, salon_id, barber_id, order_date, order_time, is_offer, total_price
          });

          for(let i=0; i<service_id.length; i++) {
            let order_service = await Order_service.create({
              order_id: orderData._id, service_id: service_id[i]
            });
            service_details.push(order_service.service_id);
          }

          let notification = await pushNotification(salon_id, {notificationType, order_id: orderData._id});
          await Notification.create({
            title: notification.title, body: notification.body, from_id: user_id, to_id: salon_id, notificationType
          });

          var userData = {
            _id: orderData._id,
            user_id: orderData.user_id,
            salon_id: orderData.salon_id,
            service_ids: service_details,
            barber_id: orderData.barber_id,
            order_date: orderData.order_date,
            order_time: orderData.order_time,
            is_offer: orderData.is_offer,
            total_price: orderData.total_price
          }
        }
        else {
          // Insert salon order details into salon_order_details table:
          let orderData = await Salon_order_details.create({
            user_id: 106, salon_id: user_id, barber_id, order_date, order_time, total_price, is_walk_in_appointment: 1,
            walk_in_user_name, walk_in_user_number, walk_in_user_cc
          });

          for(let i=0; i<service_id.length; i++) {
            let order_service = await Order_service.create({
              order_id: orderData._id, service_id: service_id[i]
            });
            service_details.push(order_service.service_id);
          }

          var userData = {
            _id: orderData._id,
            salon_id: orderData.salon_id,
            service_ids: service_details,
            barber_id: orderData.barber_id,
            order_date: orderData.order_date,
            order_time: orderData.order_time,
            total_price: orderData.total_price,
            is_walk_in_appointment: orderData.is_walk_in_appointment,
            walk_in_user_name: orderData.walk_in_user_name,
            walk_in_user_cc: orderData.walk_in_user_cc,
            walk_in_user_number: orderData.walk_in_user_number
          }
        }
        return resolve({ userData });
      } catch (error) {
        console.log('salon_order service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  user_appointments(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_status, page, limit } = body;
        let paginate = await pagination(page, limit);

        let count = await Salon_order_details.count({
          where: {user_id, order_status}
        });

        if(order_status == 2) {
          var userData = await Salon_order_details.findAll({
            include: [{
              model: User,
              as: 'salon_details',
              include: [{
                model: Salon_review,
                as: 'avg_review',
                attributes: [],
                required: true
              },{
                model: Favorite_salon,
                as: 'is_fav',
                attributes: ['is_fav'],
                where: {user_id: user_id},
                required: false
              }],
              attributes: ['_id'],
              required: true
            },{
              model: Salon_staff,
              as: 'barber_details',
              attributes: [],
              required: true
            },{
              model: Salon_service,
              as: 'service_details',
              through : { attributes: [] },
              attributes: [['_id', 'service_id'], 'name', 'time'],
              required: true
            }],
            attributes: [
              [fn('concat', profilePicDir, '/', col('salon_details.profile_pic')), 'profile_pic'],
              [col('salon_details._id'), 'salon_id'], [col('salon_details.salon_name'), 'salon_name'],
              [col('salon_details.salon_type'), 'salon_type'], [col('salon_details.email'), 'salon_email'],
              [col('salon_details.mobile'), 'salon_mobile'], [col('barber_details.name'), 'barber_name'],
              [cast(fn('ROUND', fn('AVG', col('salon_details->avg_review.avg_review')), 2), 'double precision'), 'avg_review'],
              ['_id', 'order_id'], 'order_time', 'is_offer', 'total_price', 'cancel_by',
              [fn('to_char', col('Salon_order_details.order_date'), "DD Mon YYYY"), 'order_date'],
              [fn('to_char', col('Salon_order_details.createdAt'), "DD Mon YYYY"), 'createdAt']
            ],
            where : {user_id, order_status},
            limit: paginate.limit,
            offset: paginate.offset,
            order: [['_id', 'DESC']],
            group: ['Salon_order_details._id', 'salon_details._id', 'barber_details.name']
          });
        }
        else {
          var userData = await Salon_order_details.findAll({
            include: [{
              model: User,
              as: 'salon_details',
              include: [{
                model: Salon_review,
                as: 'avg_review',
                attributes: [],
                required: true
              }],
              attributes: [],
              required: true
            },{
              model: Salon_staff,
              as: 'barber_details',
              attributes: [],
              required: true
            },{
              model: Salon_service,
              as: 'service_details',
              through : { attributes: [] },
              attributes: [['_id', 'service_id'], 'name', 'time'],
              required: true
            }],
            attributes: [
              [fn('concat', profilePicDir, '/', col('salon_details.profile_pic')), 'profile_pic'],
              [col('salon_details._id'), 'salon_id'], [col('salon_details.salon_name'), 'salon_name'],
              [col('salon_details.salon_type'), 'salon_type'], [col('salon_details.email'), 'salon_email'],
              [col('salon_details.mobile'), 'salon_mobile'], [col('barber_details.name'), 'barber_name'],
              [cast(fn('ROUND', fn('AVG', col('salon_details->avg_review.avg_review')), 2), 'double precision'), 'avg_review'],
              ['_id', 'order_id'], 'order_time', 'is_offer', 'total_price', 'cancel_by',
              [fn('to_char', col('Salon_order_details.order_date'), "DD Mon YYYY"), 'order_date'],
              [fn('to_char', col('Salon_order_details.createdAt'), "DD Mon YYYY"), 'createdAt']
            ],
            where : {user_id, order_status},
            limit: paginate.limit,
            offset: paginate.offset,
            order: [['_id', 'DESC']],
            group: ['Salon_order_details._id', 'salon_details._id', 'barber_details.name']
          });  
        }
        
        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData
        });
       
      } catch (error) {
        console.log('user_appointments service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_appointments(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, order_status, start_date, end_date, page, limit } = body;
        let where = {};
        let paginate = await pagination(page, limit);

        if(start_date) {
          where = {
            salon_id, order_status,
            order_date: { [Op.between] : [start_date, end_date] }
          }
        }
        else {
          where = { salon_id, order_status }
        }

        let count = await Salon_order_details.count({ where: where });
        
        let userData = await Salon_order_details.findAll({
          include: [{
            model: User,
            as: 'user_details',
            attributes: [],
            required: true
          },{
            model: Salon_staff,
            as: 'barber_details',
            attributes: [],
            required: true
          },{
            model: Salon_service,
            as: 'service_details',
            through : { attributes: [] },
            attributes: [['_id', 'service_id'], 'name', 'time'],
            required: true
          }],
          attributes: [
            [col('user_details._id'), 'user_id'], [col('user_details.name'), 'user_name'],
            [col('user_details.email'), 'user_email'], [col('user_details.mobile'), 'user_mobile'],
            [col('barber_details.name'), 'barber_name'],
            ['_id', 'order_id'], 'order_time', 'is_offer', 'total_price', 'cancel_by',
            'is_walk_in_appointment', 'walk_in_user_name', 'walk_in_user_number',
            [fn('to_char', col('Salon_order_details.order_date'), "DD Mon YYYY"), 'order_date'],
            [fn('to_char', col('Salon_order_details.createdAt'), "DD Mon YYYY"), 'createdAt']
          ],
          where : where,
          limit: paginate.limit,
          offset: paginate.offset,
          order: [['_id', 'DESC']],
          group: ['Salon_order_details._id', 'user_details._id', 'barber_details.name']
        });

        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData
        });
       
      } catch (error) {
        console.log('salon_appointments service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  order_completed(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, order_id } = body;
        let notificationType = 1;
        
        await Salon_order_details.update({order_status: 1},{
          where : {_id: order_id}
        });

        let userId = await Salon_order_details.findOne({
          attributes: ['user_id', 'is_walk_in_appointment'],
          where: {_id: order_id}
        });

        if(userId.is_walk_in_appointment == 0) {
          let notification = await pushNotification(userId.user_id, {notificationType, order_id});
          await Notification.create({
            title: notification.title, body: notification.body, from_id: salon_id, to_id: userId.user_id, notificationType
          });
        }

        return resolve();
      } catch (error) {
        console.log('order_completed service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  order_cancelled(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, order_id } = body;
        let cancelBy, notificationType;

        let userType = await User.findOne({
          attributes: ['user_type'],
          where: {_id: user_id}
        });

        if(userType.user_type == 1) {cancelBy=0, notificationType=2
          var userId = await Salon_order_details.findOne({
            attributes: ['salon_id', 'is_walk_in_appointment'],
            where: {_id: order_id}
          });
          var id = userId.salon_id;
        }
        else {cancelBy=1, notificationType=3
          var userId = await Salon_order_details.findOne({
            attributes: ['user_id', 'is_walk_in_appointment'],
            where: {_id: order_id}
          });
          var id = userId.user_id;
        }
        
        await Salon_order_details.update({
          order_status: 2, cancel_by: cancelBy, cancel_by_id: user_id
        }, {
          where : {_id: order_id}
        });

        if(userId.is_walk_in_appointment == 0) {
          let notification = await pushNotification(id, {notificationType, order_id});
          await Notification.create({
            title: notification.title, body: notification.body, from_id: user_id, to_id: id, notificationType
          });
        }
        
        return resolve();
      } catch (error) {
        console.log('order_cancelled service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  view_review(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, order_id } = body;
        console.log('body ==>>', body);

        let userData = await Salon_order_details.findOne({
          include: [{
            model: User,
            as: 'user_details',
            include: [{
              model: Salon_review,
              as: 'view_review',
              attributes: [],
              where: {salon_id},
              required: false
            }],
            attributes: []
          }],
          attributes: [
            [col('user_details.name'), 'user_name'], [col('user_details->view_review.user_id'), 'user_id'],
            [col('user_details->view_review.salon_id'), 'salon_id'], [col('user_details->view_review.avg_review'), 'avg_review'],
            [col('user_details->view_review.price'), 'price'], [col('user_details->view_review.friendliness'), 'friendliness'],
            [col('user_details->view_review.value'), 'value'], [col('user_details->view_review.cleanliness'), 'cleanliness'],
            [col('user_details->view_review.quality'), 'quality'], [col('user_details->view_review.createdAt'), 'createdAt'],
            [col('user_details->view_review.description'), 'description']
          ],
          where: {_id: order_id}
        })
        
        return resolve({userData});
      } catch (error) {
        console.log('view_review service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_appointment_calendar(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, date, page, limit } = body;
        let paginate = await pagination(page, limit);

        if(!date) {
          let date = new Date();
          var currentDate = moment(date).format("YYYY-MM-DD");
          console.log('currentDate ==>>', currentDate);
          body.date = currentDate;
        }
        let count = await Salon_order_details.count({
          where: {salon_id, order_status: 0, order_date: body.date}
        });
        
        let userData = await Salon_order_details.findAll({
          include: [{
            model: User,
            as: 'user_details',
            attributes: [],
            required: true
          },{
            model: Salon_staff,
            as: 'barber_details',
            attributes: [],
            required: true
          },{
            model: Salon_service,
            as: 'service_details',
            through : { attributes: [] },
            attributes: [['_id', 'service_id'], 'name', 'time'],
            required: true
          }],
          attributes: [
            [col('user_details._id'), 'user_id'], [col('user_details.name'), 'user_name'],
            [col('user_details.email'), 'user_email'], [col('user_details.mobile'), 'user_mobile'],
            [col('barber_details.name'), 'barber_name'],
            ['_id', 'order_id'], 'order_time', 'is_offer', 'total_price',
            [fn('to_char', col('Salon_order_details.order_date'), "DD Mon YYYY"), 'order_date'],
            [fn('to_char', col('Salon_order_details.createdAt'), "DD Mon YYYY"), 'createdAt']
          ],
          where : {salon_id, order_status: 0, order_date: body.date},
          limit: paginate.limit,
          offset: paginate.offset,
          order: [['_id', 'DESC']],
          group: ['Salon_order_details._id', 'user_details._id', 'barber_details.name']
        });

        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData
        });
       
      } catch (error) {
        console.log('salon_appointment_calendar service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  barber_appointment_calendar(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, barber_id, date, page, limit } = body;
        let paginate = await pagination(page, limit);

        if(!date) {
          let date = new Date();
          var currentDate = moment(date).format("YYYY-MM-DD");
          console.log('currentDate ==>>', currentDate);
          body.date = currentDate;
        }
        let count = await Salon_order_details.count({
          where: {salon_id, barber_id, order_status: 0, order_date: body.date}
        });
        
        let userData = await Salon_order_details.findAll({
          include: [{
            model: User,
            as: 'user_details',
            attributes: [],
            required: true
          },{
            model: Salon_staff,
            as: 'barber_details',
            attributes: [],
            required: true
          },{
            model: Salon_service,
            as: 'service_details',
            through : { attributes: [] },
            attributes: [['_id', 'service_id'], 'name', 'time'],
            required: true
          }],
          attributes: [
            [col('user_details._id'), 'user_id'], [col('user_details.name'), 'user_name'],
            [col('user_details.email'), 'user_email'], [col('user_details.mobile'), 'user_mobile'],
            [col('barber_details.name'), 'barber_name'],
            ['_id', 'order_id'], 'order_time', 'is_offer', 'total_price',
            [fn('to_char', col('Salon_order_details.order_date'), "DD Mon YYYY"), 'order_date'],
            [fn('to_char', col('Salon_order_details.createdAt'), "DD Mon YYYY"), 'createdAt']
          ],
          where : {salon_id, barber_id, order_status: 0, order_date: body.date},
          limit: paginate.limit,
          offset: paginate.offset,
          order: [['_id', 'DESC']],
          group: ['Salon_order_details._id', 'user_details._id', 'barber_details.name']
        });

        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData
        });
       
      } catch (error) {
        console.log('barber_appointment_calendar service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  user_device_relation(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, device_token } = body;
        let { device_id, device_type } = headers;

        let [userDeviceDetails, created] = await User_device.findOrCreate({
          where: {user_id},
          defaults: {user_id, device_token, device_id, device_type}
        });
        console.log('created ==>>', created);
        console.log('userDeviceDetails ==>>', userDeviceDetails);

        if(created == false) {
          await User_device.update({
            device_token, device_id, device_type
          }, {
            where: {user_id}
          });
        }
        return resolve();
      } catch (error) {
        console.log('user_device_relation service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  get_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, limit } = body;
        let paginate = await pagination(page, limit);

        let {count, rows} = await Notification.findAndCountAll({
          include: [{
            model: User,
            as: 'user_id',
            attributes: []
          }],
          attributes: [[fn('concat', profilePicDir, '/', col('user_id.profile_pic')), 'profile_pic'],
            '_id', 'title', 'body', 'from_id', 'to_id', 'notificationType',
            [fn('to_char', col('Notification.createdAt'), "DD-Mon-YYYY, HH12:MI AM"), 'createdAt']
          ],
          where: {to_id: user_id, is_deleted: 0},
          limit: paginate.limit,
          offset: paginate.offset,
          order: [['createdAt', 'DESC']]
        });

        let is_last_page = await checkLastPage(count, paginate.limit, page);
        return resolve({
          totalData: count,
          is_last_page: is_last_page,
          userData: rows
        });
      } catch (error) {
        console.log('get_notification service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  delete_notification(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, notification_id } = body;

        await Notification.update({is_deleted: 1},{
          where: {_id: notification_id}
        });
        return resolve();
      } catch (error) {
        console.log('delete_notification service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  check_order() {
    return new Promise(async (resolve, reject) => {
      try {
        let date = new Date();
        let currentTime = date.toLocaleTimeString(undefined, {
          hour:   '2-digit',
          minute: '2-digit'
        });
        let currentDate = moment(date).format("YYYY-MM-DD");

        let checkOrder = await Salon_order_details.findAll({
          attributes: ['_id', 'salon_id'],
          where: {order_status: 0,
            order_date: {
              [Op.lte]: currentDate
            },
            order_time: {
              [Op.lt]: currentTime
            }
          }
        });

        if(checkOrder != null) {
          for(let i=0; i<checkOrder.length; i++) {
            await Salon_order_details.update(
              {order_status: 2, cancel_by: 1, cancel_by_id: checkOrder[i].salon_id},
              { where: {_id: checkOrder[i]._id} }
            );
          }
        }

        return resolve();
      } catch (error) {
        console.log('check_order service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  my_earnings(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, start_date, end_date, is_offer, page, limit } = body;
        let paginate = await pagination(page, limit);
        let where = {};
        let total_earnings;

        if(is_offer) {
          where = {
            salon_id, order_status: 1, is_offer, order_date: {[Op.between] : [start_date, end_date]}
          }
        }
        else {
          where = {
            salon_id, order_status: 1, order_date: {[Op.between] : [start_date, end_date]}
          }
        }

        let myEarnings = await Salon_order_details.findOne({
          attributes: [[fn('SUM', col('total_price')), 'myEarn']],
          where: where,
          group: ['Salon_order_details.total_price']
        });

        if(myEarnings == null) {total_earnings = 0}
        else {total_earnings = myEarnings.dataValues.myEarn}

        let {count, rows} = await Salon_order_details.findAndCountAll({
          include: [{
            model: User,
            as: 'user_details',
            attributes: []
          }],
          attributes: [[literal('"user_details"."_id"'), '_id'], [literal('"user_details"."name"'), 'name'],
            'total_price', [fn('to_char', col('Salon_order_details.order_date'), "DD-MM-YYYY"), 'order_date']
          ],
          where: where,
          limit: paginate.limit,
          offset: paginate.offset,
          order: [['order_date', 'DESC']]
        });

        if(count == 0) {var is_last_page = true}
        else {var is_last_page = await checkLastPage(count, paginate.limit, page);}

        return resolve({
          totalData: count,
          total_earnings,
          is_last_page,
          userData: rows
        });
      } catch (error) {
        console.log('my_earnings service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  user_dashboard(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id } = body;

        let userData = await User.findAll({
          include: [{
            model: Salon_order_details,
            as: 'appointments',
            attributes: []
          }],
          attributes: ['_id', 'name', 'email', 'country_code', 'mobile',
            [col('appointments.order_status'), 'order_status'],
            [fn('COUNT', col('appointments.order_status')), 'count']
          ],
          where: {_id: user_id},
          raw: true,
          group: ['User._id', 'appointments.order_status']
        });
        console.log('userData ==>>', userData);

        return resolve({
          userData: {
            _id: userData[0]._id,
            name: userData[0].name,
            email: userData[0].email,
            country_code: userData[0].country_code,
            mobile: userData[0].mobile,
            upcoming_appointments: parseInt(userData[0].count),
            completed_appointments: parseInt(userData[1].count),
            cancelled_appointments: parseInt(userData[2].count),
            total_appointments: parseInt(userData[1].count) + parseInt(userData[2].count)
          }
        });
      } catch (error) {
        console.log('user_dashboard service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  salon_dashboard(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id } = body;

        let total_app_user = await User.count({
          where: {is_deleted: 0}
        })

        let total_app_appointments = await Salon_order_details.count({
          where: {order_status: 1}
        })

        let {count, rows} = await User.findAndCountAll({
          include: [{
            model: Salon_review,
            as: 'avg_review',
            attributes: []
          }],
          attributes: [[fn('concat', profilePicDir, '/', col('User.profile_pic')), 'profile_pic'],
            [cast(fn('ROUND', fn('AVG', col('avg_review.avg_review')), 2), 'DOUBLE PRECISION'), 'avg_review'],
            '_id', 'salon_name', 'name', 'salon_type', 'email', 'country_code', 'mobile'
          ],
          where: {_id: salon_id},
          raw: true,
          group: ['User._id']
        });

        let offers = await Salon_offers.findAll({
          include: [{
            model: Salon_service,
            as: 'service_details',
            attributes: []
          }],
          attributes: [[fn('concat', offerImageDir, '/', col('offer_image')), 'offer_image'],
            '_id', 'discount', [col('service_details.name'), 'service_name']
          ],
          where: {salon_id, is_deleted: 0}
        })

        if(rows[0].avg_review ==  null) {
          return resolve({
            userData: {
              _id: rows[0]._id,
              profile_pic: rows[0].profile_pic,
              avg_review: rows[0].avg_review,
              total_review: 0,
              salon_name: rows[0].salon_name,
              name: rows[0].name,
              salon_type: rows[0].salon_type,
              email: rows[0].email,
              country_code: rows[0].country_code,
              mobile: rows[0].mobile,
              total_app_user,
              total_app_appointments,
              offers: offers
            }
          });
        }

        return resolve({
          userData: {
            _id: rows[0]._id,
            profile_pic: rows[0].profile_pic,
            avg_review: rows[0].avg_review,
            total_review: count[0].count,
            salon_name: rows[0].salon_name,
            name: rows[0].name,
            salon_type: rows[0].salon_type,
            email: rows[0].email,
            country_code: rows[0].country_code,
            mobile: rows[0].mobile,
            total_app_user,
            total_app_appointments,
            offers: offers
          }
        });
      } catch (error) {
        console.log('salon_dashboard service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  add_offer(body, filename) {
    return new Promise(async (resolve, reject) => {
      try {
        let { salon_id, service_id, discount, description } = body;
        body.offer_image = filename

        let userData = await Salon_offers.create(body)
        console.log('userData ===>>', userData);
        return resolve();
      }
      catch(error) {
        console.log('add_offer service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  get_offer(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { user_id, page, limit } = body;
        let where = {};
        let paginate = await pagination(page, limit);

        let userType = await User.findOne({
          attributes: ['user_type'],
          where: {_id: user_id}
        });

        if(userType.user_type == 1) {
          where = {is_deleted: 0}  
        }
        else {
          where = {salon_id: user_id, is_deleted: 0}
        }

        let {count, rows} = await Salon_offers.findAndCountAll({
          include: [{
            model: User,
            as: 'salon_details',
            attributes: []
          },{
            model: Salon_service,
            as: 'service_details',
            attributes: []
          }],
          attributes: [[fn('concat', offerImageDir, '/', col('offer_image')), 'offer_image'],
            '_id', 'salon_id', 'service_id', 'discount', 'description',
            [col('salon_details.salon_name'), 'salon_name'], [col('service_details.name'), 'service_name']
          ],
          where: where,
          limit: paginate.limit,
          offset: paginate.offset
        });

        if(count == 0) {var is_last_page = true}
        else {var is_last_page = await checkLastPage(count, paginate.limit, page);}
        return resolve({
          totalData: count,
          is_last_page,
          userData: rows
        });
      }
      catch(error) {
        console.log('get_offer service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

  dlt_offer(body, headers) {
    return new Promise(async (resolve, reject) => {
      try {
        let { _id } = body;
        let offerImageDirPath = path.join(__dirname + offerImagePath);

        let offerImage = await Salon_offers.findOne({
          attributes: ['offer_image'],
          where: {_id}
        });
        console.log('offerImage ===>>', offerImage);

        await Salon_offers.destroy({
          where: {_id}
        });

        fs.unlinkSync(offerImageDirPath + offerImage.offer_image);
        return resolve();
      }
      catch(error) {
        console.log('dlt_offer service catch error ===>>', error.message);
        return reject(error);
      }
    });
  }

}

module.exports = new UserService();