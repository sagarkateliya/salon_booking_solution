const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const config = require('../config');
const path = require('path');
const moment = require('moment');
const firebase = require('../config/firebase');
const {User_device} = require('../models');
const { notificationData } = require('../config/notification');

class Common {
  getJwtToken(user_id) {
    return new Promise((resolve, reject) => {
      try {
        let expirationTime = config.jwtExpiryUserTime,
        sign = {
          user_id: user_id
        };

        let token = jwt.sign(sign, config.jwtSecretKey, {
          expiresIn: expirationTime
        });
        return resolve(token);
      }
      catch(error) {
        return reject(error);
      }
    });
  };

  generateOTP(n) {
    let digits = '0123456789';
    let otp = '';

    for (let i = 0; i < n; i++) {
      let index = Math.floor(Math.random() * digits.length);

      if(i == 0 && !parseInt(digits[index]))
        i--;
      else 
        otp += digits[index];
    }
    return otp;
  };

  encryptedPassword(password) {
    const hash = bcrypt.hash(password, 10);
    return hash;
  }

  async decryptedPassword(password, stored_password) {
    const result = await bcrypt.compare(password, stored_password);
    return result;
  }

  profile_pic(image) {
    let profile_pic =  path.format({
      root: '/ignored',
      dir: config.profilePicDir,
      base: image
    });
    return profile_pic;
  }

  sendMails(user) {
    const transport = nodemailer.createTransport({
      host: config.sendMailHost,
      port: config.sendMailPort,
      auth: {
        user: config.sendMailAuthUser,
        pass: config.sendMailAuthPass
      }
    });

    if('otp' in user == true) {
      return transport.sendMail({
        from: 'tristate.mteam@gmail.com',
        to: user.email,
        subject: 'OTP',
        html: `<div>
          <h1>You succesfully signed up!</h1>
          <p>Signup OTP:</p>
          <h1>${user.otp}</h1>
        </div>`
      });
    }

    return transport.sendMail({
      from: 'tristate.mteam@gmail.com',
      to: config.sendMailToEmail,
      subject: user.subject,
      html: `<div>
        <h1>User contact details</h1>
        <h4>Name: ${user.name}</h4>
        <h4>Email: ${user.email}</h4>
        <h4>Mobile number: ${user.country_code} ${user.mobile}</h4>
        <h4>Message: ${user.message}</h4>
      </div>`
    });
  }

  convertTimeFormat(time) {
    if (time == null || time == "") {
      return time
    }
    var hrs = Number(time.match(/^(\d+)/)[1]);
    var mnts = Number(time.match(/:(\d+)/)[1]);
    var format = time.match(/\s(.*)$/)[1];

    if (format == "PM" && hrs < 12) hrs = hrs + 12;
    if (format == "AM" && hrs == 12) hrs = hrs - 12;

    var hours = hrs.toString();
    var minutes = mnts.toString();

    if (hrs < 10) hours = "0" + hours;
    if (mnts < 10) minutes = "0" + minutes;

    var timeend = hours + ":" + minutes + ":00";
    return timeend;
  }

  time_slots(work_str_time, work_end_time, brk_str_time, brk_end_time) {
    let time_slots = [];

    const w_start_time = moment(work_str_time, 'hh:mm A');
    const w_end_time = moment(work_end_time, 'hh:mm A');
    const b_start_time = moment(brk_str_time, 'hh:mm A');
    const b_end_time = moment(brk_end_time, 'hh:mm A');
    console.log('w_start_time ==>>', w_start_time);
    console.log('w_end_time ==>>', w_end_time);
    console.log('b_start_time ==>>', b_start_time);
    console.log('b_end_time ==>>', b_end_time);

    if(brk_str_time == '') {
      while(w_start_time.isBefore(w_end_time)) {
        time_slots.push(w_start_time.format('hh:mm A'));
        w_start_time.add(10, 'm');
      }  
    }
    else {
      while(w_start_time.isBefore(b_start_time)) {
        time_slots.push(w_start_time.format('hh:mm A'));
        w_start_time.add(10, 'm');
      }
      while(b_end_time.isBefore(w_end_time)) {
        time_slots.push(b_end_time.format('hh:mm A'));
        b_end_time.add(10, 'm')
      }
    }

    console.log('time_slots ==>>', time_slots);
    return time_slots;
  }

  pagination(page, limit) {
    return {
      limit: limit || 10,
      offset: (page-1) * (limit || 10)
    }
  }

  checkLastPage(totalData, limit, page) {
    let totalPage = Math.ceil(totalData/limit);

    if(totalPage != page) {return false}
    else {return true}
  }

  async pushNotification(user_id, data) {
    let device_token = await User_device.findOne({
      attributes: ['device_token'],
      where: {user_id}
    });

    let token = device_token.device_token;
    let notification = await notificationData(data.notificationType)
    
    await firebase.sendFCMPushNotification(token, notification, data);
    return notification;
  }

}

module.exports = new Common();