const {sequelize} = require('../models/index');

class DB {
  Database() {
    return new Promise(async (resolve, reject) => {
      try {
        await sequelize.authenticate();
        console.log('== Pgsql Connected ==');
      }
      catch(error) {
        console.log(`\nDatabase catch error ->> ${error}`);
        return reject(error);
      }
    });
  };
};

module.exports = new DB();