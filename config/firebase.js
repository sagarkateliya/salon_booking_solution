const FCM = require('fcm-node');
const server_key = "AAAAiJdnSho:APA91bHnaR0oOQ1M6hkMxOMXAF00J4I1gnrTR8FMx2eYuU10JQoR2iFeUkeThmB2y_viroG77sWTWQveD0Fh8VfkyOqp8I0Xv2TsRSvKmKC_btj4-0wr1Wa5xyLbDrXvdYamTP9SxCtf"
var fcm = new FCM(server_key);

class firebase {
  sendFCMPushNotification(token, notification, data) {
    return new Promise(async (resolve, reject) => {
      try{
        const notificationData = {
          to: token,
          collapse_key: 'type_a',
          notification: notification,
          data: data,
          android: {
            priority: "high",
            // ttl: 10 60 1000
          }
        };
        console.log('notificationData ==>>', notificationData);
    
        await fcm.send(notificationData, function(error, response){
          if(error) {
            console.log('sendFCMPushNotification error ==>>', error);
            return reject(error);
          }
          console.log('response ==>>', response);
          return resolve(response);
        })
      }
      catch(error) {
        console.log('sendFCMPushNotification error ===>>', error.message);
        return reject(error);
      }
    });
  }
}

module.exports = new firebase()