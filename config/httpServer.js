const http = require('http');
const https = require('https');
const fs = require('fs');
const app = require('../server');
const path = require('path');

module.exports = () => {
  return new Promise(async (resolve, reject) => {
    try {
      server = http.Server(app);
      return resolve();
    }
    catch(error) {
      console.log('\nhttpServer catch error ==>>', error);
      return reject(error);
    }
  });
}