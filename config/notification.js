class notification {
  notificationData(type) {
    return new Promise(async (resolve, reject) => {
      try {
        if(type == 1) {
          return resolve({
            title: 'Order complete',
            body: 'Your appointment was successfully completed'
          });
        }
        else if(type == 2) {
          return resolve({
            title: 'Order cancelled',
            body: 'Order was cancelled by customer'
          });
        }
        else if(type == 3) {
          return resolve({
            title: 'Order cancelled',
            body: 'Order was cancelled by salon'
          });
        }
        else if(type == 4) {
          return resolve({
            title: 'New order coming',
            body: 'Clicked to show more order details'
          });
        }
      }
      catch(error) {
        console.log('notificationData catch error ===>>', error.message);
        return reject(error);
      }
    })
  }
}

module.exports = new notification();