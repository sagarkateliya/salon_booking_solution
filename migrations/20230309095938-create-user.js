'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('users', {
      _id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_type: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 1,
        comment: '1:user, 2:salon'
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true
      },
      country_code: {
        type: Sequelize.STRING,
        allowNull: true
      },
      mobile: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        allowNull: true
      },
      gender: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        comment: '1:male, 2:female'
      },
      salon_type: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 1,
        comment: '1:hair, 2:nail'
      },
      profile_pic: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      salon_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      address: {
        type: Sequelize.STRING,
        allowNull: true
      },
      service_gender: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        comment: '1:male, 2:female, 3:both'
      },
      is_active: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 1,
        comment: '0,1'
      },
      is_deleted: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: '0,1'
      },
      is_approved: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: '0,1'
      },
      otp: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      is_verified: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: '0,1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('users');
  }
};