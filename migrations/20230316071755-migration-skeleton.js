'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('salon_business_hours', 'schedule_type', {
          type: Sequelize.SMALLINT,
          allowNull: false,
          defaultValue: 1,
          comment: '1:weekly schedule, 2:daily schedule'
        }, { transaction: t }),
        queryInterface.addColumn('salon_business_hours', 'date', {
          type: Sequelize.DATEONLY,
          allowNull: true
        }, { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('salon_business_hours', 'schedule_type', { transaction: t }),
        queryInterface.removeColumn('salon_business_hours', 'date', { transaction: t })
      ]);
    });
  }
};
