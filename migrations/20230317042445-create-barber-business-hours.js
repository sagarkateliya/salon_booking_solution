'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('barber_business_hours', {
      _id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      salon_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'users',
            schema: 'public'
          },
          key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      barber_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'salon_staff',
            schema: 'public'
          },
          key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      schedule_type: {
        type: Sequelize.SMALLINT,
        allowNull: false,
        defaultValue: 1,
        comment: '1:weekly schedule, 2:daily schedule'
      },
      day: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      working_start_time: {
        type: Sequelize.STRING,
        allowNull: true
      },
      working_end_time: {
        type: Sequelize.STRING,
        allowNull: true
      },
      break_start_time: {
        type: Sequelize.STRING,
        allowNull: true
      },
      break_end_time: {
        type: Sequelize.STRING,
        allowNull: true
      },
      available: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: '0:Barber available, 1:Barber not available'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('barber_business_hours');
  }
};