'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('salon_review', {
      _id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'users',
            schema: 'public'
          },
          key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      salon_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: 'users',
            schema: 'public'
          },
          key: '_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      },
      price: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Review between 0 to 5'
      },
      value: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Review between 0 to 5'
      },
      quality: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Review between 0 to 5'
      },
      friendliness: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Review between 0 to 5'
      },
      cleanliness: {
        type: Sequelize.SMALLINT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Review between 0 to 5'
      },
      avg_review: {
        type: Sequelize.FLOAT,
        allowNull: true,
        defaultValue: 0,
        comment: 'Average of above five columns value'
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('salon_review');
  }
};