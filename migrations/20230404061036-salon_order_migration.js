'use strict';

const { type } = require('os');
const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('salon_order_details', 'total_price', {
          type: Sequelize.FLOAT,
          allowNull: true
        }, { transaction: t }),
        queryInterface.removeColumn('salon_order_details', 'service_id', { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('salon_order_details', 'total_price', { transaction: t }),
        queryInterface.addColumn('salon_order_details', 'service_id', {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: {
              tableName: 'salon_service',
              schema: 'public'
            },
            key: '_id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE'
        }, { transaction: t }),
      ]);
    });
  }
};
