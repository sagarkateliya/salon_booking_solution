'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('salon_order_details', 'cancel_by_id', {
          type: Sequelize.SMALLINT,
          allowNull: true
        }, { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('salon_order_details', 'cancel_by_id', { transaction: t })
      ]);
    });
  }
};
