'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('notification', 'is_deleted', {
          type: Sequelize.SMALLINT,
          allowNull: true,
          defaultValue: 0,
          comment: '0,1'
        }, { transaction: t }),
        queryInterface.addColumn('notification', 'notificationType', {
          type: Sequelize.INTEGER,
          allowNull: true
        }, { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('notification', 'is_deleted', { transaction: t }),
        queryInterface.removeColumn('notification', 'notificationType', { transaction: t })
      ]);
    });
  }
};
