'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('salon_order_details', 'is_walk_in_appointment', {
          type: Sequelize.SMALLINT,
          allowNull: true,
          defaultValue: 0,
          comment: '0: online order, 1: offline order'
        }, { transaction: t }),
        queryInterface.addColumn('salon_order_details', 'walk_in_user_name', {
          type: Sequelize.STRING,
          allowNull: true
        }, { transaction: t }),
        queryInterface.addColumn('salon_order_details', 'walk_in_user_number', {
          type: Sequelize.STRING,
          allowNull: true
        }, { transaction: t }),
        queryInterface.changeColumn('salon_order_details', 'user_id', {
          type: Sequelize.INTEGER,
          allowNull: true 
        }, { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('salon_order_details', 'is_walk_in_appointment', { transaction: t }),
        queryInterface.removeColumn('salon_order_details', 'walk_in_user_name', { transaction: t }),
        queryInterface.removeColumn('salon_order_details', 'walk_in_user_number', { transaction: t }),
        queryInterface.changeColumn('salon_order_details', 'user_id', {
          type: Sequelize.INTEGER,
          allowNull: false
        }, { transaction: t })
      ]);
    });
  }
};
