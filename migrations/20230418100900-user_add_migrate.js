'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('users', 'latitude', {
          type: Sequelize.FLOAT,
          allowNull: true
        }, { transaction: t }),
        queryInterface.addColumn('users', 'longitude', {
          type: Sequelize.FLOAT,
          allowNull: true
        }, { transaction: t })
      ]);
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('users', 'latitude', { transaction: t }),
        queryInterface.removeColumn('users', 'longitude', { transaction: t })
      ]);
    });
  }
};
