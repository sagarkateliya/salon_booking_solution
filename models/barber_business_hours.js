'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Barber_business_hours extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Barber_business_hours.init({
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    barber_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    schedule_type: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    day: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    working_start_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    working_end_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    break_start_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    break_end_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    available: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'barber_business_hours',
    modelName: 'Barber_business_hours',
    timestamps: true
  });
  return Barber_business_hours;
};