'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Salon_business_hours extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Salon_business_hours.belongsTo(models.User, {
        foreignKey: 'salon_id',
        as: 'users'
      })
    }
  }
  Salon_business_hours.init({
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    salon_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    schedule_type: {
      type: DataTypes.SMALLINT,
      allowNull: false
    },
    day: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    working_start_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    working_end_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    break_start_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    break_end_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    available: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'salon_business_hours',
    modelName: 'Salon_business_hours',
    timestamps: true
  });
  return Salon_business_hours;
};