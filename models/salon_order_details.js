'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Salon_order_details extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Salon_order_details.hasOne(models.Order_service, {
        foreignKey: 'order_id',
        as: 'order_details'
      });

      Salon_order_details.belongsTo(models.User, {
        foreignKey: 'salon_id',
        as: 'salon_details'
      });

      Salon_order_details.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user_details'
      });

      Salon_order_details.belongsTo(models.Salon_staff, {
        foreignKey: 'barber_id',
        as: 'barber_details'
      });

      Salon_order_details.belongsToMany(models.Salon_service, {
        through: 'order_service',
        as: 'service_details',
        foreignKey: 'order_id',
        otherKey: 'service_id'
      });
    }
  }
  Salon_order_details.init({
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    salon_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    barber_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    order_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    order_time: {
      type: DataTypes.STRING,
      allowNull: true
    },
    order_status: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    is_offer: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    total_price: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    cancel_by: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    cancel_by_id: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    is_walk_in_appointment: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    walk_in_user_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    walk_in_user_number: {
      type: DataTypes.STRING,
      allowNull: true
    },
    walk_in_user_cc: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'salon_order_details',
    modelName: 'Salon_order_details',
    timestamps: true
  });
  return Salon_order_details;
};