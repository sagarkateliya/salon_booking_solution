'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Salon_review extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      Salon_review.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user_details'
      })

      Salon_review.belongsTo(models.User, {
        foreignKey: 'salon_id',
        as: 'salon_details'
      })
    }
  }
  Salon_review.init({
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    salon_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    price: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    value: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    quality: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    friendliness: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    cleanliness: {
      type: DataTypes.SMALLINT,
      allowNull: true
    },
    avg_review: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'salon_review',
    modelName: 'Salon_review',
    timestamps: true
  });
  return Salon_review;
};