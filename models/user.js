'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Salon_service, {
        foreignKey: 'salon_id',
        as: 'salon_service'
      })

      User.hasMany(models.Salon_staff, {
        foreignKey: 'salon_id',
        as: 'barber_details'
      })

      User.hasOne(models.Favorite_salon, {
        foreignKey: 'salon_id',
        as: 'is_fav'
      })

      User.hasMany(models.Salon_review, {
        foreignKey: 'salon_id',
        as: 'salon_reviews'
      })

      User.hasOne(models.Salon_review, {
        foreignKey: 'salon_id',
        as: 'avg_review'
      })

      User.hasOne(models.Salon_review, {
        foreignKey: 'user_id',
        as: 'view_review'
      })

      User.hasMany(models.Salon_images_videos, {
        foreignKey: 'salon_id',
        as: 'salon_gallery'
      })

      User.hasOne(models.Barber_business_hours, {
        foreignKey: 'salon_id',
        as: 'barber_time'
      })

      User.hasOne(models.Salon_order_details, {
        foreignKey: 'user_id',
        as: 'appointments'
      })

      User.hasMany(models.Salon_offers, {
        foreignKey: 'salon_id',
        as: 'offer_details'
      })
    }
  }
  User.init({
    _id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_type: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    },
    country_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    gender: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    salon_type: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    profile_pic: {
      type: DataTypes.STRING,
      allowNull: true
    },
    salon_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    latitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    longitude: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    service_gender: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    is_active: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    is_deleted: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    is_approved: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    otp: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    is_verified: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    referral_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'users',
    modelName: 'User',
    timestamps: true
  });

  User.addScope('distance', (latitude, longitude, distance, unit = 'km') => {
    const constant = unit == 'km' ? 6371 : 3959;
    const haversine = `(
      ${constant} * acos(
        cos(radians(${latitude}))
        * cos(radians(latitude))
        * cos(radians(longitude) - radians(${longitude}))
        + sin(radians(${latitude})) * sin(radians(latitude))
      )
    )`;

    return {
      attributes: [
        [sequelize.literal(haversine), 'distance']
      ],
      where: sequelize.where(sequelize.literal(haversine), '<=', distance)
    }
  })
  
  return User;
};