require('express-group-routes');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const path = require('path');
const cron = require('node-cron');
const { Database } = require('./config/db');

app.set('port', process.env.PORT || 3000);

app.use(
  bodyParser.json({
    limit: '50mb'
  })
);

app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  })
);

app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/storage/salon_image_video', express.static(path.join(__dirname, 'storage/salon_image_video')));
app.use('/storage/offer_images', express.static(path.join(__dirname, 'storage/offer_images')));
app.set('views', path.join(__dirname, 'views'));

module.exports = app;
require('./app/routes/index').routerConfig(app);

cron.schedule("*/30 * * * *", () => {
  require('./app/routes/index').cronConfig(app);
})

Promise.all([require('./config/httpServer')()])
  .then((values) => {
    server.listen(app.get('port'), () => {
      console.log(
        ` ----------------------- Server listening on the port ${app.get(
          'port'
        )} ${new Date()} ----------------------- `
      );

      Database();
    });
  })
  .catch((error) => {
    console.log(
      `----------------------- Main server configuration error >> ${error} \n---------------------------------------------- `
    );
  })