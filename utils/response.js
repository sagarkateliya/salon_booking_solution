class Response {
  async error(res, msg, language, statusCode = 400) {

    let response = {
      code: 0,
      status: 'FAIL',
      message: msg,
    }

    if (msg == 'USER_BLOCKED')
      statusCode = 403;

    if (msg == 'TOKEN_EXPIRED')
      statusCode = 401;

    if (msg === 'UPGRADE_APP')
      statusCode = 403;

    res.status(statusCode).json(response);
  }

  async success(res, msg, language, data, statusCode = 200) {
    let response = {
      code: 1,
      status: 'SUCCESS',
      message: msg,
      data: data ? data : {}
    }

    res.status(statusCode).json(response);
  }
}

module.exports = new Response();